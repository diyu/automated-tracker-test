# Import necessary packages
from pymeasure.instruments.keithley import Keithley2400
import numpy as np
import pandas as pd
from time import sleep

# Set the input parameters
data_points = 50
averages = 10
max_current = 0.001
min_current = -max_current

# Set source_current and measure_voltage parameters
current_range = 10e-3  # in Amps
compliance_voltage = 10  # in Volts
measure_nplc = 0.1  # Number of power line cycles
voltage_range = 1  # in VOlts

# Connect and configure the instrument
sourcemeter_v = Keithley2400("GPIB0::18::INSTR")
sourcemeter_i = Keithley2400("GPIB0::25::INSTR")
# sourcemeter_v.reset()
sourcemeter_v.write("*RST")
sourcemeter_i.write("*RST")

print("00    ", sourcemeter_v.ask("*IDN?"))
print("01    ", sourcemeter_i.ask("*IDN?"))

sourcemeter_v.write(":SOUR:FUNC VOLT")
sourcemeter_i.write("CONFigure:CURRent[:DC]")
# sourcemeter_v.apply_voltage()

# sourcemeter_v.compliance_current = 5e-6
sourcemeter_v.write(":SOUR:VOLT:ILIMIT 5e-6")

# print(sourcemeter_v.check_get_errors())
# print(sourcemeter_v.check_errors())
# exit(0)
sleep(0.1)  # wait here to give the instrument time to react
# sourcemeter_v.stop_buffer()
# sourcemeter_v.disable_buffer()

# Allocate arrays to store the measurement results
currents = np.linspace(min_current, max_current, num=data_points)
voltages = np.zeros_like(currents)
voltage_stds = np.zeros_like(currents)

sourcemeter_v.enable_source()
# sourcemeter_v.write("OUTPUT ON")

sleep(1.0)


# For IV test

for voltage in range(0,-10,-1):
    sourcemeter_v.source_voltage = float(voltage)
    current = float(sourcemeter_i.ask(":READ?"))

    print("voltage",voltage,"current",current)

    sleep(1.0)
# sourcemeter_v.write("OUTPUT OFF")
sourcemeter_v.shutdown()
#exit(0)
# sourcemeter_v.voltage = 0.0







# # Loop through each current point, measure and record the voltage
# for i in range(data_points):
#     sourcemeter_v.config_buffer(averages)
#     sourcemeter_v.source_current = currents[i]
#     sourcemeter_v.start_buffer()
#     sourcemeter_v.wait_for_buffer()
#     # Record the average and standard deviation
#     voltages[i] = sourcemeter_v.means[0]
#     sleep(1.0)
#     voltage_stds[i] = sourcemeter_v.standard_devs[0]

# Save the data columns in a CSV file
# data = pd.DataFrame({
#     'Current (A)': currents,
#     'Voltage (V)': voltages,
#     'Voltage Std (V)': voltage_stds,
# })
# data.to_csv('example.csv')
#
exit(0)