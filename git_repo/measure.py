# Import necessary packages
# Developed by USTC
from pymeasure.instruments.keithley import Keithley2470
from pymeasure.instruments.keithley import Keithley6482
# from pymeasure.instruments.agilent import AgilentE4980
from pymeasure.instruments.instrument import FakeInstrument
import numpy as np
#import pandas as pd
from time import sleep
import time
import sys
import os
from math import *
import winsound
#import matplotlib.pyplot as plt
#import monitor as mon
# if you use `from measure import *` to use this file, run init() before measerment

exitFlag = False
continueFlag = False
frequency = 10e3

IoutCmdListV2 = [
    # IoutA   IoutB  IoutC  IoutD
    ["GND" , "CH13", "GND" , "GND" ], # 00
    ["CH9" , "CH8" , "CH3" , "CH4" ], # 01
    ["CH10", "CH7" , "CH2" , "CH5" ], # 02
    ["CH26", "CH6" , "CH1" , "CH15"], # 03
    ["CH25", "CH16", "CH11", "CH20"], # 04
    ["CH24", "CH12", "CH21", "CH14"], # 05
    ["CH18", "CH17", "CH22", "CH19"], # 06
    ["GND" , "CH23", "GND" , "GND" ], # 07
    ["GND" , "GND" , "GND" , "GND" ], # 08
    ["GND" , "GND" , "GND" , "GND" ], # 09
]


IoutCmdListV3 = [
    # IoutA   IoutB  IoutC  IoutD
    ["GND", "GND" , "GND" , "GND" ], # 00
    ["CH1", "CH8" , "CH15", "CH22"], # 01
    ["CH2", "CH9" , "CH16", "CH23"], # 02
    ["CH3", "CH10", "CH17", "CH24"], # 03
    ["CH4", "CH11", "CH18", "CH25"], # 04
    ["CH5", "CH12", "CH19", "CH26"], # 05
    ["CH6", "CH13", "CH20", "GND" ], # 06
    ["CH7", "CH14", "CH21", "GND" ], # 07
    ["GND", "GND" , "GND" , "GND" ], # 08
    ["GND", "GND" , "GND" , "GND" ], # 09
]

IoutCmdList15x15 = [
    # IoutA  IoutB  IoutC   IoutD   IoutE   IoutF   IoutG   IoutH   
    ["GND", "GND" , "GND" , "GND" , "GND" , "GND" , "GND" , "GND" ], # 00
    ["CH211" , "CH212" , "CH213" , "CH214" , "CH215" , "CH216" , "CH217" , "GND" ], # 01
    ["CH196" , "CH197" , "CH198" , "CH199" , "CH200" , "CH201" , "CH202" , "GR01" ], # 02
    ["CH181" , "CH182" , "CH183" , "CH184" , "CH185" , "CH186" , "CH187" , "GND" ], # 03
    ["CH166" , "CH167" , "CH168" , "CH169" , "CH170" , "CH171" , "CH172" , "GND" ], # 04
    ["CH151" , "CH152" , "CH153" , "CH154" , "CH155" , "CH156" , "CH157" , "CH203" ], # 05
    ["CH136" , "CH137" , "CH138" , "CH139" , "CH140" , "CH141" , "CH142" , "CH173" ], # 06
    ["CH121" , "CH122" , "CH123" , "CH124" , "CH125" , "CH126" , "CH127" , "CH143" ], # 07
    ["CH106" , "CH107" , "CH108" , "CH109" , "CH110" , "CH111" , "CH112" , "GND" ], # 08
    ["CH091" , "CH092" , "CH093" , "CH094" , "CH095" , "CH096" , "CH097" , "CH113" ], # 09
    ["CH076" , "CH077" , "CH078" , "CH079" , "CH080" , "CH081" , "CH082" , "CH083" ], # 10
    ["CH061" , "CH062" , "CH063" , "CH064" , "CH065" , "CH066" , "CH067" , "CH053" ], # 11
    ["CH046" , "CH047" , "CH048" , "CH049" , "CH050" , "CH051" , "CH052" , "CH023" ], # 12
    ["CH031" , "CH032" , "CH033" , "CH034" , "CH035" , "CH036" , "CH037" , "GND" ], # 13
    ["CH016" , "CH017" , "CH018" , "CH019" , "CH020" , "CH021" , "CH022" , "GND" ], # 14
    ["CH001" , "CH002" , "CH003" , "CH004" , "CH005" , "CH006" , "CH007" , "GND" ], # 15
    ["CH015" , "CH014" , "CH013" , "CH012" , "CH011" , "CH010" , "CH009" , "GND" ], # 16
    ["CH030" , "CH029" , "CH028" , "CH027" , "CH026" , "CH025" , "CH024" , "GND" ], # 17
    ["CH045" , "CH044" , "CH043" , "CH042" , "CH041" , "CH040" , "CH039" , "GND" ], # 18
    ["CH060" , "CH059" , "CH058" , "CH057" , "CH056" , "CH055" , "CH054" , "CH008" ], # 19
    ["CH075" , "CH074" , "CH073" , "CH072" , "CH071" , "CH070" , "CH069" , "CH038" ], # 20
    ["CH090" , "CH089" , "CH088" , "CH087" , "CH086" , "CH085" , "CH084" , "CH068" ], # 21
    ["CH105" , "CH104" , "CH103" , "CH102" , "CH101" , "CH100" , "CH099" , "CH098" ], # 22
    ["CH120" , "CH119" , "CH118" , "CH117" , "CH116" , "CH115" , "CH114" , "GND" ], # 23
    ["CH135" , "CH134" , "CH133" , "CH132" , "CH131" , "CH130" , "CH129" , "CH128" ], # 24
    ["CH150" , "CH149" , "CH148" , "CH147" , "CH146" , "CH145" , "CH144" , "CH158" ], # 25
    ["CH165" , "CH164" , "CH163" , "CH162" , "CH161" , "CH160" , "CH159" , "CH188" ], # 26
    ["CH180" , "CH179" , "CH178" , "CH177" , "CH176" , "CH175" , "CH174" , "CH218" ], # 27
    ["CH195" , "CH194" , "CH193" , "CH192" , "CH191" , "CH190" , "CH189" , "GND" ], # 28
    ["CH210" , "CH209" , "CH208" , "CH207" , "CH206" , "CH205" , "CH204" , "GND" ], # 29
    ["CH225" , "CH224" , "CH223" , "CH222" , "CH221" , "CH220" , "CH219" , "GND" ], # 30
    ["GND", "GND" , "GND" , "GND" , "GND" , "GND" , "GND" , "GND" ], # 31
]

SwitchBoardVersion = "V3"

if SwitchBoardVersion == "V2":
    IoutCmdList = IoutCmdListV2
elif SwitchBoardVersion == "V3":
    IoutCmdList = IoutCmdListV3

def init():
    """    
    initialize and configure SMU, #SMU_LV, LCR
    """
    global SMU,LCR,SMU_LV 
    global delay,frequency, ac_voltage, precision, voltage_step, measure_average_times, voltage_min , voltage_fin
    global guardRingGround, doStable, current_compliance, voltage_now,current_now, withLCR,withSMU,withSMULV, voltage_breakdown, useChannelB
    global voltage_now_chA,voltage_now_chB,Cp_now,Rp_now
    global measureSpeed # [s/V]
    global totalTimeUsed
    global monBufferX,monBufferY,monStableX,monStableY,monHVX,monHVY
    #------Configuration    
    delay = 1.0 # seconds
    voltage_min = 0
    voltage_fin = 0
    #frequency = 1e3 # 1e3 for irradiated sensors, 1e4 for non-irradiated sensors   
    #ac_voltage = 0.1
    #ac_voltage = 0.5
    ac_voltage = 1.0
    precision = 1e-3
    voltage_step = 1
    #measure_average_times = 3
    #measure_average_times = 2
    measure_average_times = 1
    guardRingGround = 1
    doStable = 0
    
    monBufferX = []
    monBufferY = []
    monStableX = []
    monStableY = []
    monHVX = []
    monHVY = []
    
    if 'current_compliance' not in globals(): 
        current_compliance = 5e-6
    
    #current_threshold = 
    #current_threshold = 50e-6 * 0.9 
    voltage_now = -1
    current_now = 0
    voltage_breakdown = -1
    totalTimeUsed ="n/a"
    withSMU = True
    withLCR = True
    withSMULV = True
    useChannelB = True
    voltage_now_chA = 0
    voltage_now_chB = 0
    measureSpeed = -1.0
    Cp_now = -1
    Rp_now = -1
    
    try:
        #SMU = Keithley2400("GPIB1::24::INSTR")
        SMU = Keithley2400("GPIB0::24::INSTR")
    except Exception as exc:
        print("Warning: HV source 2410 initialized FAILED",type(exc),exc.args)
        withSMU=False
    else:
        withSMU=True
        
    
    #SMU = Keithley2400("GPIB0::24::INSTR")
    
    #SMU_LV = Keithley2400("GPIB0::26::INSTR")
    try:
        LCR = AgilentE4980("GPIB2::17::INSTR")
    except Exception as exc:
        print("Warning: LCR initialized FAILED",type(exc),exc.args)
        withLCR=False
    else:
        withLCR=True
    
    useSMULV = True
    try:
        SMU_LV = Keithley2400("GPIB0::25::INSTR")   # for 6482
        #SMU_LV = Keithley2400("GPIB0::26::INSTR")  # for 2636 
        
        print(SMU_LV.write("*RST"))

        print(SMU_LV.write(":SOUR1:VOLT 0"))
        print(SMU_LV.write(":SOUR2:VOLT 0"))
        # print(SMU_LV.write(":SOUR2:VOLT 1"))

        print(SMU_LV.write(":OUTP1 ON"))
        print(SMU_LV.write(":OUTP2 ON"))
        
    except Exception as exc:
        print("Warning: SMU LV initialized FAILED",type(exc),exc.args)
        withSMULV=False
    else:
        withSMULV=True

    #------ Say Hello to each Instrument
    print("Saying Hello to each Instrument:")
    
    #------ Connect Instrument 
    
    if withSMU: 
        SMU.reset()
        print(SMU.ask("*IDN?"))
    
    sleep(0.1) # wait here to give the instrument time to react
    
    if withSMULV: 
        SMU_LV.reset()        
        print(SMU_LV.ask("*IDN?"))
        # print(SMU_LV.write(":SENS1:CURR:RANG 2E-6"))
        # print(SMU_LV.write(":SENS2:CURR:RANG 2E-6"))
        
        
        SMU_LV.write(":SYSTem:LFRequency 50")
        SMU_LV.write(":SENS2:CURR:NPLCycles 5") # Current Recommen           dation NPLC=5
        #SMU_LV.write(":SYSTem:AZERo:STATe ON")

        SMU_LV.write(":SENS1:CURR:RANG 2E-12")
        #SMU_LV.write(":SENS1:CURR:RANG 2E-8")
        SMU_LV.write(":SENS1:CURR:RANG:AUTO ON")
        SMU_LV.write(":SENS1:CURR:RANG:AUTO:ULIM 500e-6")
        #SMU_LV.write(":SENS1:CURR:RANG:AUTO:ULIM 20e-3")
        
        #SMU_LV.write(":SENS1:CURR:RANG:AUTO:LLIM 2e-11")
        #SMU_LV.write(":SENS1:CURR:RANG:AUTO:LLIM 2e-12")
        SMU_LV.write(":SENS2:CURR:RANG:AUTO:LLIM 2e-14")


        SMU_LV.write(":SENS2:CURR:RANG 2E-12")
        #SMU_LV.write(":SENS2:CURR:RANG 2E-8")
        SMU_LV.write(":SENS2:CURR:RANG:AUTO ON")
        SMU_LV.write(":SENS2:CURR:RANG:AUTO:ULIM 500e-6")
        #SMU_LV.write(":SENS2:CURR:RANG:AUTO:LLIM 2e-11")
        SMU_LV.write(":SENS2:CURR:RANG:AUTO:LLIM 2e-14")
        #SMU_LV.write(":SENS1:CURR:RANG:AUTO:ULIM 20e-3")
        #Set grounded enable when using TRX 
        #SMU_LV.write(":SOURce1:GCONnect ON")     
        #SMU_LV.write(":SOURce2:GCONnect ON") 
        
        #Set grounded disable when using VSource 
        SMU_LV.write(":SOURce1:GCONnect OFF")     
        SMU_LV.write(":SOURce2:GCONnect OFF") 

        

        SMU_LV.write(":DISP:DIG 5")
    #------Initialize LCR
    # Mode:
    #* CPRP: Parallel capacitance [F] and parallel resistance [Ohm]
    #* CPD: Parallel capacitance [F] and dissipation factor [number]

    if withLCR:
        LCR.reset()
        print(LCR.ask("*IDN?"))
        LCR.mode = "CPRP"
        #LCR.mode = "CPQ"  
        LCR.frequency=frequency
        LCR.ac_voltage=ac_voltage
    
    
    #------Initialize SMU
    if withSMU:
        
        SMU.apply_voltage()
        #SMU.apply_voltage() Todo:NPLC
        #SMU.compliance_current = 5e-3 # For IV measurement complance should be 5e-6  for CV with HV module should be 1e-3
        SMU.compliance_current = current_compliance
    #------Initialize #SMU_LV 2636 style
    #if withSMULV:
        #SMU_LV.ask("*IDN?")
        #SMU_LV.write("smua.source.levelv=0") #set voltage=0
        #SMU_LV.write("display.smua.measure.func=0") #set measuremode=current
        #SMU_LV.write("display.smua.digits=4")
        #Auto Range with low limit
        #SMU_LV.write("smua.measure.autorangei=smua.AUTORANGE_ON") # turn off auto range
        #SMU_LV.write("smua.measure.lowrangei=1E-8") #set measuremode=current
        #if useChannelB:    
        #SMU_LV.write("smub.source.levelv=0") #set voltage=0
        #SMU_LV.write("display.smub.measure.func=0") #set measuremode=current
        #SMU_LV.write("display.screen=display.SMUA_SMUB") #set measuremode=current
        #SMU_LV.write("display.smub.digits=4")
        #Auto Range with low limit
        #SMU_LV.write("smub.measure.autorangei=smub.AUTORANGE_ON") # turn off auto range
        #SMU_LV.write("smub.measure.lowrangei=1E-8") #set measuremode=current


    #Mannual Range
    ##SMU_LV.write("smua.measure.autorangei=smua.AUTORANGE_OFF") # turn off auto range
    ##SMU_LV.write("smua.measure.rangei=1E-7") #set measuremode=current

    print("Initialization Finished.") 


def LCRCorr(status="OPEN"):
    if status=="OPEN" or status == "open":
        LCR.write(":CORRection:OPEN:STATe ON")
        LCR.write(":CORRection:OPEN:EXECute")
    elif status=="SHORt" or status== "short" or status =="SHORT":
        LCR.write(":CORRection:SHORt:STATe ON")
        LCR.write(":CORRection:SHORt:EXECute")
    else:
        print("Usage: LCRCorr('OPEN') or LCRCorr('SHORt')")

def timeUtil(fmt="txt"):
    if fmt == "r": #raw time
        return time.time()
    elif fmt == "txt": #text time
        return time.asctime( time.localtime(time.time()) )
    elif fmt == "f": #full time
        #return "NULL"
        return " Time:" + str(time.asctime( time.localtime(time.time()) )) + "\t stamp:" + str(time.time())
    else:
        return "ERR."    
def setVoltage(voltage, ch="HV"):
    global voltage_now, voltage_now_chA, voltage_now_chB,monHVY,monHVX
    if ch == "HV":
        if voltage_now == abs(voltage):
            print("Warning!! the voltage to set(",voltage,") is same as current voltage(",voltage_now,") would do nothing")
            return
        voltage_now = abs(voltage)

        if (voltage>0): voltage=-voltage #-HV
        #if (voltage<0): voltage=-voltage # +HV!!!!!!
        if withSMU:
            SMU.source_voltage = voltage
            SMU.enable_source()
            SMU.measure_current()
        # SMU_LV.write(":SOUR2:VOLT 1")
        monHVY.append(voltage_now)
        monHVX.append(time.time())
        return

    if (voltage<0): voltage=-voltage  
    if (voltage>100): 
        print("WARNING!!! voltage for LV exceed 100V, V=",voltage," ignored!")
        return
    
    if ch == "chA":
        if withSMULV:
            SMU_LV.write(":SOUR1:VOLT -"+str(voltage))
            # SMU_LV.write("smua.source.levelv="+str(voltage))
        voltage_now_chA = voltage
        return

    if ch == "chB":
        if withSMULV:
            SMU_LV.write("smub.source.levelv="+str(voltage))
        voltage_now_chB = voltage
        return


def MeasureImpedance():
    global current_now,monBufferX,monBufferY,Cp_now,Rp_now
    #print("point 1 "+str(measure_average_times))
    subdelay=round(delay/measure_average_times,4)
    #subdelay=0.02
    results1=[]
    results2=[]
    #print("point 2")
    if withSMU:
        current_now=SMU.current
    #print("point 3")
    #SMU_LV.write("smua.measure.i()")
    for n in range(measure_average_times):
        if withLCR:
            tbefore = time.time()
            imp=LCR.impedance
            tafter = time.time()
            print("read out LCR.. taking ",tafter-tbefore," seconds")
        else:
            imp = [cos(voltage_now),sin(voltage_now)]
        
        Cp_now = float(imp[0])
        Rp_now = float(imp[1])
        
        monBufferX.append(int(time.time()))
        monBufferY.append(float(imp[0]))
        results1.append(float(imp[0]))
        results2.append(float(imp[1]))
    #    print("point 3."+str(n)+".a  "+str(subdelay))
        sleep(subdelay)
    #    print("point 3."+str(n)+".b")
     
    return (sum(results1)/len(results1), sum(results2)/len(results2)) # (Cp,Rp)

def MeasureCurrent():
    global current_now
    #subdelay=round(delay/measure_average_times,4)
    tMeasureCurrentStart = time.time()
    subdelay=0.02
    #subdelay=0.05
    #subdelay=0.12
    results1=[]
    results2=[]
    results3=[]
    
    for n in range(measure_average_times):
        sleep(subdelay)
        tMeasureCurrentNStart = time.time()

        tMeasureCurrentNReadHVStart = time.time()
        results1.append(float(SMU.current))
        tMeasureCurrentNReadHVEnd = time.time()
        print("MeasureCurrent[",n,"]Read HV executed ..... takes",tMeasureCurrentNReadHVEnd - tMeasureCurrentNReadHVStart," seconds.")

        current_now=results1[n]
        print("n:",n,"SMU1:",results1[n],timeUtil("f"),"subdelay:",subdelay)
        if guardRingGround:
            tMeasureCurrentNReadStart = time.time()
            rtn = SMU_LV.ask(":READ?")
            tMeasureCurrentNReadEnd = time.time()
            print("MeasureCurrent[",n,"]Read  executed ..... takes",tMeasureCurrentNReadEnd - tMeasureCurrentNReadStart," seconds.");
            LV_currents = rtn.split(",",1)
            #results2.append(float(SMU_LV.ask("print(smua.measure.i())"))) #could consider only measure once #SMU_LV when LV high delay
            #results2.append(0.0)
            results2.append(-1.0 * float(LV_currents[0]))
            #results3.append(float(SMU_LV.ask("print(smub.measure.i())")))
            results3.append(-1.0 * float(LV_currents[1]))
            #results3.append(1e-11)

            monBufferX.append(int(time.time()))
            monBufferY.append(-1.0 * float(LV_currents[0]))

        else:
            results2.append(0.0)
            results3.append(0.0)
        #sleep(subdelay)
        print("n:",n,"#SMU_LV:",results2[n],results3[n],timeUtil("f"),"subdelay:",subdelay)
        tMeasureCurrentNEnd = time.time()
        print("MeasureCurrent[",n,"]  executed ..... takes",tMeasureCurrentNEnd - tMeasureCurrentNStart," seconds."); 

    tMeasureCurrentEnd = time.time()
    print("MeasureCurrent:",sum(results1)/len(results1),timeUtil("f")) 
    print("MeasureCurrent executed ..... takes",tMeasureCurrentEnd - tMeasureCurrentStart," seconds."); 
    return (sum(results1)/len(results1), sum(results2)/len(results2),sum(results3)/len(results3)) # (I,IGR)

def isStable(v1,v2,scale="rel"):
    global monStableX,monStableY
    
    print("isStable: Rel:", abs(v1-v2)/v1, "Abs:", abs(v1-v2),timeUtil("f"))
    monStableY.append(abs(v1-v2)/v1)
    monStableX.append(time.time())
    
    if scale == "rel":    return abs( (v1-v2) / v1) < precision
    elif scale == "abs":  return abs(v1-v2) < precision
    else: print("Wrong scale:",scale); return False

def MeasureOnceImpedance(voltage,delay=1):
    return MeasureOnce(voltage,"C",delay)

def MeasureOnceCurrent(voltage,delay=1):
    return MeasureOnce(voltage,"I",delay)

def MeasureOnce(voltage,target="I",delay=1):
    tMeasureOnceSetVoltageStart = time.time() 
    setVoltage(voltage)
    tMeasureOnceSetVoltageEnd = time.time()
    print("setVoltage to ",voltage," executed..... takes",tMeasureOnceSetVoltageEnd - tMeasureOnceSetVoltageStart," seconds.")

    print ("MeasureOnce, delay: ",delay) 
    sleep(delay) 
    print ("MeasureOnce, delay: ",delay,"...done")
    tMeasureOnceStart = time.time()
    if target == "I": 
        val = MeasureCurrent()
    elif target == "C": 
        val = MeasureImpedance()
    else: 
        print ("ERROR: WRONG Target:",target); 
        return -1
    tMeasureOnceEnd = time.time()
    print("MeasureOnce executed..... takes",tMeasureOnceEnd - tMeasureOnceStart," seconds.")
    if not doStable:
        #SMU_LV.write("beeper.beep(0.3, 432)")
        return val

    index = 0
    while index < 100:
        if target == "I": val_new = MeasureCurrent()
        else:
            val_new = MeasureImpedance(); 
            # #SMU_LV.write("smua.measure.i()"); SMU.current
        if(isStable(val[0], val_new[0])):
            print("isstable!",val_new)
            if withSMULV:
                SMU_LV.ask(":READ?")
                #SMU_LV.write("beeper.beep(0.3, 440)")
            return val_new
        index = index + 1
        val = val_new
    return "Outtime"
        
def MeasureOnceCVI(voltage,target="I",delay=1):
    setVoltage(voltage)
    sleep(0.5) # Wait time before meas.	500 ms
    val = MeasureCurrent()
    #if not doStable:
    #    SMU_LV.write("beeper.beep(0.3, 432)")
    return val  
    
def MeasureCV(voltage_max,filename="cvlist.csv"):
    global voltage_step, precision, doStable, useChannelB, exitFlag
    global SMU#,SMU_LV
    #SMU_LV.write("smua.source.output=smua.OUTPUT_ON")
    if useChannelB:
        SMU_LV.write("smub.source.output=smub.OUTPUT_ON")
    SMU.compliance_current = current_compliance # with HV modual 
    #precision=1e-3
    #voltage_step=0.5
    #voltage_step=0.25
    #doStable=1
    #voltage_min = 55.8 #type 3.2
    #voltage_min = 31 #type 3.1
    #voltage_min = 0
    try:
        with open(filename,"w") as file:
            file.write("CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{}\n".format(frequency,ac_voltage,delay,precision,timeUtil())) 
            file.write("V\t,Cp\t,Rp\n")
            #file.write("V\t,Cp\t,Rp\t,I\t,I_GR\n")
            voltage = 0.0
            voltage_step_tmp = voltage_step
            while voltage < voltage_max :
            # for voltage in np.arange(0,voltage_max,voltage_step):
                voltage=round(voltage,3)
                if voltage < voltage_min:
                    if voltage < int(voltage_min):
                        if (voltage - int(voltage)) > 0:
                            voltage = voltage + voltage_step
                            continue
                        setVoltage(voltage)
                        sleep(0.1)
                        voltage = voltage + voltage_step
                        continue
                    setVoltage(voltage)
                    sleep(voltage_step*0.1)
                    voltage = voltage + voltage_step
                    continue

                if voltage >= 36 and voltage < 45:
                    voltage_step = 0.2
                else:
                    voltage_step = voltage_step_tmp

                impedance_result = MeasureOnce(voltage,"C")
                if impedance_result == "Outtime":
                    print("Waiting for stable result out of time")
                    break
                file.write("{}, {}, {}\n".format(-voltage,impedance_result[0],impedance_result[1]))
                file.flush()
                print("-------------V=",voltage,"Cp=",impedance_result[0],"Rp=",impedance_result[1],timeUtil())
                voltage = voltage + voltage_step
                if exitFlag:
                    print("exit signal received, exiting now....")
                    exitFlag = False
                    break
                
    except KeyboardInterrupt as exc:
        print("Warning: CV Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown()



def MeasurePara5x5(voltage_max,filename="cvlist.csv",IoutMode = "A",MeasureMode = "C"):
   
    print("start to measure 5x5 switch at IoutMode = ",IoutMode)
    global voltage_step, precision, doStable, useChannelB, exitFlag, measureSpeed, voltage_breakdown, totalTimeUsed
    #global SMU
    
    if withSMU:
        SMU.compliance_current = current_compliance # with HV modual 
    
    #precision=1e-3
    #doStable=0
    # IoutList = []
    # if IoutMode == "Iout A" : IoutList.append(0) ;
    # if IoutMode == "Iout B" : IoutList.append(1) ;
    # if IoutMode == "Iout C" : IoutList.append(2) ;
    # if IoutMode == "Iout D" : IoutList.append(3) ;    
    # if IoutMode == "All" : 
    #     IoutList.append(0) 
    #     IoutList.append(1) 
    #     IoutList.append(2) 
    #     IoutList.append(3)    

    # print("start to measure 5x5 switch at Iout = ",IoutMode,"IoutList = ",IoutList)
    
    padList = []
    CHAList = [1,18]
    fileList = []
    swCmdList = []
    Iout = 0
    
# for Iout in IoutList:
    for m in range(1,27):
    # for m in CHAList:
        print('mode:', m)
        # PUT = IoutCmdList[m][Iout]
        if MeasureMode == 'CV':
            if m == 26:
                continue
        if m == 26:
            PUT = "GND"
        else:
            PUT = "CH" + str(m)
        print('mode:', m, ' PUT:', PUT)
        
        # if PUT == 'GND' or PUT == 'GR' or  PUT == 'CH26':
        #     print('PUT is ', PUT, '  continue....')
        #     continue
        
        pad = str(m)
        
        pad_raw = pad
        if len(pad) == 1: 
            pad = "0" + pad  
        newfile = filename[0:filename.rfind('/')+1] + pad + ".csv"    
        print('measuring pad:', PUT, 'save as:', newfile)
        
        if os.path.exists(newfile):
            if False : # doOverwrite
                print("file:",newfile,"already exists, overwrite is enabled, old file is deleted......")
                cmd = "del "+newfile.replace("/","\\")
                os.system(cmd)
                pass
            else:
                print("file:",newfile,"already exists, overwrite not enabled, continue......")
                continue
        ch = int(pad)
        GNDStr = "1111"
        
        GNDStr = GNDStr[:Iout] + "0" + GNDStr[Iout+1:]
        
        #swCmd = r"python swboard.py "+str(m)+" 4 4 " + GNDStr
        if SwitchBoardVersion == "V2":
            swCmd = r"python swboard.py "+str(m)+" "+str(Iout)+" 4 " + GNDStr
        elif SwitchBoardVersion =="V3":
            swCmd = r"python swboard.py "+ str(m)
        else:
            print("Warning, Invalid Switch Board Version :", SwitchBoardVersion, "Switch command is empty")

        padList.append(pad)
        fileList.append(newfile)
        swCmdList.append(swCmd)

    try:
        firstFlag = True
        pass_threshold_count = 0
        tWholeTotalBefore = time.time()
        nPointCount = -1
        # for voltage in np.arange(0,voltage_max,voltage_step):
        voltage = 0.0
        voltage_step_tmp = voltage_step
        while voltage < voltage_max:
            nPointCount = nPointCount + 1
            
            # remove half of points in 0-40V range for the Fast HPK 5x5 probe
            #if voltage > 0.1 and voltage < 40:
            #    if nPointCount % 2 == 1:
            #        continue

            voltage=round(voltage,3)
            if voltage < voltage_min:
                if voltage < int(voltage_min):
                    if (voltage - int(voltage)) > 0:
                        voltage = voltage + voltage_step
                        continue
                    setVoltage(voltage)
                    sleep(1.0)
                    voltage = voltage + voltage_step
                    continue
                setVoltage(voltage)
                sleep(voltage_step*1.0)
                voltage = voltage + voltage_step
                continue
            
            # if voltage >= 36 and voltage < 53:
            #     voltage_step = 0.2
            # else:
            #     voltage_step = voltage_step_tmp
            # if voltage >=18:
            #     voltage_step = 2.0
            # if voltage >=20:
            #     voltage_step = voltage_step_tmp
            
            result=[]
            for i  in range(0,len(padList)):
                tTotalBefore = time.time()
                
                if firstFlag:
                    if MeasureMode == 'CV':
                        os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
                        os.system("echo V\t,Cp\t,Rp >> {}".format(fileList[i]))
                    if MeasureMode == 'IV':
                        os.system("echo CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(voltage_step,guardRingGround,delay,precision,timeUtil(),fileList[i]))
                        os.system("echo V\t,I\t,I_P1\t,I_P2 >> {}".format(fileList[i]))                        
                    
                print("exec cmd:",swCmdList[i])
                tSwitchBefore = time.time()
                os.system(swCmdList[i])
                tSwitchEnd = time.time()
                print("switch executed, takes ",tSwitchEnd - tSwitchBefore," seconds")
                rdelay = delay
                # if i == 0:
                #     rdelay = delay
                # else:
                #     rdelay = 0.01

                tMeasBefore = time.time()
                if MeasureMode == 'CV':
                    result = MeasureOnce(voltage,"C",rdelay)
                if MeasureMode == 'IV':
                    result = MeasureOnce(voltage,"I",rdelay)
                tMeasEnd = time.time()
                print("measured pad:", padList[i], "at voltage ", voltage," takes ",tMeasEnd - tMeasBefore," seconds")

                tSaveBefore = time.time()
                if MeasureMode == 'CV':
                    os.system("echo {}, {}, {} >> {}".format(-voltage,result[0],result[1],fileList[i]))
                if MeasureMode == 'IV':
                    os.system("echo {}, {}, {}, {} >> {}".format(-voltage,result[0],result[1],result[2],fileList[i]))
                tSaveEnd = time.time()
                print("save result to file:",fileList[i], "at voltage ", voltage," takes ",tSaveEnd - tSaveBefore," seconds")
                
                tTotalEnd = time.time()
                if MeasureMode == 'CV':
                    print("-------------V=",voltage,"Cp=",result[0],"Rp=",result[1],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds" )
                if MeasureMode == 'IV':
                    print("-------------V=",voltage,"I=",result[0],"I_P1=",result[1],"I_P2=",result[2],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds")
                
                measureSpeed = round((tTotalEnd - tTotalBefore)/(1.0*voltage_step),3)
            print("Finished voltage---",voltage,"result",result,"exitFlag:",exitFlag,"firstFlag:",firstFlag)        
            firstFlag = False
            if MeasureMode == "IV" and max(abs(result[1]), abs(result[0]), abs(result[2])) >= current_compliance * 0.9:
                if voltage_breakdown < 0 :
                    voltage_breakdown = voltage
                pass_threshold_count=pass_threshold_count+1

            if pass_threshold_count > 0:
                tWholeTotalEnd = time.time()
                tTotalUsed = round(tWholeTotalEnd - tWholeTotalBefore,2)
                totalTimeUsed = str(tTotalUsed)+" s ("+str(round(tTotalUsed/60.0,1))+" min)"
                print("measure finished, total takes: ",totalTimeUsed)
                break
        
            if exitFlag:
                print("exit signal received, exiting now....")
                exitFlag = False
                break
            voltage = voltage + voltage_step
            
        for i in range(0,len(padList)):
            if MeasureMode == 'CV':
                os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
            if MeasureMode == 'IV':
                os.system("echo CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(voltage_step,guardRingGround,delay,precision,timeUtil(),fileList[i]))
            
    except KeyboardInterrupt as exc:
        print("Warning: ",MeasureMode," Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown()



def MeasurePara5x5CV(voltage_max,filename="cvlist.csv",IoutMode = "A"):
   
    print("start to measure 5x5 switch at IoutMode = ",IoutMode)
    global voltage_step, precision, doStable, useChannelB, exitFlag, measureSpeed, voltage_breakdown, totalTimeUsed, voltage_now
    #global SMU
    
    if withSMU:
        SMU.compliance_current = current_compliance # with HV modual 
    
    padList = []
    CHAList = [19, 22]
    fileList = []
    swCmdList = []
    Iout = 0
    
# for Iout in IoutList:
    # for m in range(1,26):
    for m in CHAList:
        print('mode:', m)
       
        PUT = "CH" + str(m)
        print('mode:', m, ' PUT:', PUT)
        
        pad = str(m)
        
        pad_raw = pad
        if len(pad) == 1: 
            pad = "0" + pad  
        newfile = filename[0:filename.rfind('/')+1] + pad + ".csv"    
        print('measuring pad:', PUT, 'save as:', newfile)
        
        if os.path.exists(newfile):
            if False : # doOverwrite
                print("file:",newfile,"already exists, overwrite is enabled, old file is deleted......")
                cmd = "del "+newfile.replace("/","\\")
                os.system(cmd)
                pass
            else:
                print("file:",newfile,"already exists, overwrite not enabled, continue......")
                continue
        ch = int(pad)
        GNDStr = "1111"
        
        GNDStr = GNDStr[:Iout] + "0" + GNDStr[Iout+1:]
        
        #swCmd = r"python swboard.py "+str(m)+" 4 4 " + GNDStr
        if SwitchBoardVersion == "V2":
            swCmd = r"python swboard.py "+str(m)+" "+str(Iout)+" 4 " + GNDStr
        elif SwitchBoardVersion =="V3":
            swCmd = r"python swboard.py "+ str(m)
        else:
            print("Warning, Invalid Switch Board Version :", SwitchBoardVersion, "Switch command is empty")

        padList.append(pad)
        fileList.append(newfile)
        swCmdList.append(swCmd)

    try:
        firstFlag = True
        pass_threshold_count = 0
        tWholeTotalBefore = time.time()
        nPointCount = -1
        voltage_step_tmp = voltage_step
        # for voltage in np.arange(0,voltage_max,voltage_step):
        for i in range(0,len(padList)):
            tTotalBefore = time.time()
            print("exec cmd:",swCmdList[i])
            tSwitchBefore = time.time()
            os.system(swCmdList[i])
            tSwitchEnd = time.time()
            print("switch executed, takes ",tSwitchEnd - tSwitchBefore," seconds")
            rdelay = delay
            voltage = 0.0
            
            while voltage < voltage_max:
                nPointCount = nPointCount + 1
                
                # remove half of points in 0-40V range for the Fast HPK 5x5 probe
                #if voltage > 0.1 and voltage < 40:
                #    if nPointCount % 2 == 1:
                #        continue

                voltage=round(voltage,3)
                if voltage < voltage_min:
                    if voltage < int(voltage_min):
                        if (voltage - int(voltage)) > 0:
                            voltage = voltage + voltage_step
                            continue
                        setVoltage(voltage)
                        sleep(1.0)
                        voltage = voltage + voltage_step
                        continue
                    setVoltage(voltage)
                    sleep(voltage_step*1.0)
                    voltage = voltage + voltage_step
                    continue
                
                if voltage >= 36 and voltage < 53:
                    voltage_step = 0.2
                else:
                    voltage_step = voltage_step_tmp
                # if voltage >=18:
                #     voltage_step = 2.0
                # if voltage >=20:
                #     voltage_step = voltage_step_tmp
                
                result=[]

                
                
                if firstFlag:
                    os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
                    os.system("echo V\t,Cp\t,Rp >> {}".format(fileList[i]))
                    
                
                # if i == 0:
                #     rdelay = delay
                # else:
                #     rdelay = 0.01

                tMeasBefore = time.time()
                result = MeasureOnce(voltage,"C",rdelay)
                if result == "Outtime":
                    break
                tMeasEnd = time.time()
                print("measured pad:", padList[i], "at voltage ", voltage," takes ",tMeasEnd - tMeasBefore," seconds")

                tSaveBefore = time.time()
                os.system("echo {}, {}, {} >> {}".format(-voltage,result[0],result[1],fileList[i]))
                
                tSaveEnd = time.time()
                print("save result to file:",fileList[i], "at voltage ", voltage," takes ",tSaveEnd - tSaveBefore," seconds")
                
                tTotalEnd = time.time()
                print("-------------V=",voltage,"Cp=",result[0],"Rp=",result[1],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds" )
                
                measureSpeed = round((tTotalEnd - tTotalBefore)/(1.0*voltage_step),3)



                print("Finished voltage---",voltage,"result",result,"exitFlag:",exitFlag,"firstFlag:",firstFlag)        
                firstFlag = False
                
            
                if exitFlag:
                    print("exit signal received, exiting now....")
                    exitFlag = False
                    break
                voltage = voltage + voltage_step

            for voltage in np.arange(voltage_now, -0.2,-12.0):  # [max,min)
        #    for voltage in np.arange(voltage_now, -0.2,-1.0):  # [max,min)
                print("RAMPDOWN-------------v=",voltage,"---voltage_now=",voltage_now)
                setVoltage(voltage)
                
                #if withSMU:
                    #current_now=SMU.current
                sleep(0.1)
            setVoltage(0.0)
            sleep(1.0)

        for i in range(0,len(padList)):
            os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
            
    except KeyboardInterrupt as exc:
        print("Warning: ",MeasureMode," Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown()




def MeasurePara15x15(voltage_max,filename="cvlist.csv",IoutMode = "A",MeasureMode = "C"):
   
    print("start to measure 5x5 switch at IoutMode = ",IoutMode)
    global voltage_step, precision, doStable, useChannelB, exitFlag, measureSpeed, voltage_breakdown, totalTimeUsed
    #global SMU
    if withSMU:
        SMU.compliance_current = current_compliance # with HV modual 
    
    #precision=1e-3
    #doStable=0
    # IoutList = []
    # if IoutMode == "Iout A" : IoutList.append(0) ;
    # if IoutMode == "Iout B" : IoutList.append(1) ;
    # if IoutMode == "Iout C" : IoutList.append(2) ;
    # if IoutMode == "Iout D" : IoutList.append(3) ;    
    # if IoutMode == "All" : 
    #     IoutList.append(0) 
    #     IoutList.append(1) 
    #     IoutList.append(2) 
    #     IoutList.append(3)    

    # print("start to measure 5x5 switch at Iout = ",IoutMode,"IoutList = ",IoutList)
    
    padList = []
    fileList = []
    swCmdList = []
    Iout = 0
    Padpara = ['A','B','C','D','E','F','G','H']
    Chlist = [0,7]
    
    for para in range(0,8):
    # for para in Chlist:
        for m in range(0,32):
        # for m in [2]:
            # if len(str(m)) == 1:
            #     PUT = Padpara[para] + "0" + str(m)
            # else:
            #     PUT = Padpara[para] + str(m)
            # print('mode:', PUT)
            PUT = IoutCmdList15x15[m][para]
            
            if PUT == 'GND' :
                print('PUT is ', PUT, '  continue....')
                continue
            
            if len(str(m)) == 1:
                pad = Padpara[para] + "0" + str(m)
            else:
                pad = Padpara[para] + str(m)
            print('mode:', PUT)
            # pad = PUT.replace("CH","")
            
            pad_raw = pad
            if len(pad) == 1: 
                pad = "0" + pad  
            newfile = filename[0:filename.rfind('/')+1] + pad + ".csv"    
            print('measuring pad:', PUT, 'save as:', newfile)
            
            if os.path.exists(newfile):
                if False : # doOverwrite
                    print("file:",newfile,"already exists, overwrite is enabled, old file is deleted......")
                    cmd = "del "+newfile.replace("/","\\")
                    os.system(cmd)
                    pass
                else:
                    print("file:",newfile,"already exists, overwrite not enabled, continue......")
                    continue
            # ch = int(pad)
            GNDStr = "1111"
            
            GNDStr = GNDStr[:Iout] + "0" + GNDStr[Iout+1:]
            
            #swCmd = r"python swboard.py "+str(m)+" 4 4 " + GNDStr
            if SwitchBoardVersion == "V2":
                swCmd = r"python swboard.py "+str(m)+" "+str(Iout)+" 4 " + GNDStr
            elif SwitchBoardVersion =="V3":
                swCmd = r"python swboard.py "+str(para)+" "+str(m)
            else:
                print("Warning, Invalid Switch Board Version :", SwitchBoardVersion, "Switch command is empty")

            padList.append(pad)
            fileList.append(newfile)
            swCmdList.append(swCmd)

    try:
        firstFlag = True
        pass_threshold_count = 0
        tWholeTotalBefore = time.time()
        nPointCount = -1
        voltage = 0
        # for voltage in np.arange(0,voltage_max,voltage_step):
        while voltage <= voltage_max:    
            nPointCount = nPointCount + 1
            
            # remove half of points in 0-40V range for the Fast HPK 5x5 probe
            #if voltage > 0.1 and voltage < 40:
            #    if nPointCount % 2 == 1:
            #        continue

            voltage=round(voltage,3)
            if voltage < voltage_min:
                if voltage < int(voltage_min):
                    if (voltage - int(voltage)) > 0:
                        voltage = voltage + voltage_step
                        continue
                    setVoltage(voltage)
                    sleep(5.0)
                    voltage = voltage + voltage_step
                    continue
                setVoltage(voltage)
                sleep(voltage_step*5.0)
                voltage = voltage + voltage_step
                continue
            
            if MeasureMode == 'CV':
                if voltage > 45:
                    voltage_step = 1
                if voltage > 70:
                    voltage_step = 3


            result=[]
            for i  in range(0,len(padList)):
                tTotalBefore = time.time()
                
                if firstFlag:
                    if MeasureMode == 'CV':
                        os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
                        os.system("echo V\t,Cp\t,Rp >> {}".format(fileList[i]))
                    if MeasureMode == 'IV':
                        os.system("echo CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(voltage_step,guardRingGround,delay,precision,timeUtil(),fileList[i]))
                        os.system("echo V\t,I\t,I_P1\t,I_P2 >> {}".format(fileList[i]))                        
                    
                print("exec cmd:",swCmdList[i])
                tSwitchBefore = time.time()
                os.system(swCmdList[i])
                tSwitchEnd = time.time()
                print("switch executed, takes ",tSwitchEnd - tSwitchBefore," seconds")
                # rdelay = 1.0
                # if i == 0:
                #     rdelay = delay
                # else:
                #     rdelay = 1.0

                rdelay = 1.0
                tMeasBefore = time.time()
                if MeasureMode == 'CV':
                    result = MeasureOnce(voltage,"C",rdelay)
                if MeasureMode == 'IV':
                    result = MeasureOnce(voltage,"I",rdelay)
                tMeasEnd = time.time()
                print("measured pad:", padList[i], "at voltage ", voltage," takes ",tMeasEnd - tMeasBefore," seconds")

                tSaveBefore = time.time()
                if MeasureMode == 'CV':
                    os.system("echo {}, {}, {} >> {}".format(-voltage,result[0],result[1],fileList[i]))
                if MeasureMode == 'IV':
                    os.system("echo {}, {}, {}, {} >> {}".format(-voltage,result[0],result[1],result[2],fileList[i]))
                tSaveEnd = time.time()
                print("save result to file:",fileList[i], "at voltage ", voltage," takes ",tSaveEnd - tSaveBefore," seconds")
                
                tTotalEnd = time.time()
                if MeasureMode == 'CV':
                    print("-------------V=",voltage,"Cp=",result[0],"Rp=",result[1],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds" )
                if MeasureMode == 'IV':
                    print("-------------V=",voltage,"I=",result[0],"I_P1=",result[1],"I_P2=",result[2],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds")
                
                measureSpeed = round((tTotalEnd - tTotalBefore)/(1.0*voltage_step),3)
            print("Finished voltage---",voltage,"result",result,"exitFlag:",exitFlag,"firstFlag:",firstFlag)        
            firstFlag = False
            
            if MeasureMode == "IV" and max(abs(result[1]), abs(result[0]), abs(result[2])) >= current_compliance * 0.9:
                if voltage_breakdown < 0 :
                    voltage_breakdown = voltage
                pass_threshold_count=pass_threshold_count+1

            if pass_threshold_count>=1:
                tWholeTotalEnd = time.time()
                tTotalUsed = round(tWholeTotalEnd - tWholeTotalBefore,2)
                totalTimeUsed = str(tTotalUsed)+" s ("+str(round(tTotalUsed/60.0,1))+" min)"
                print("measure finished, total takes: ",totalTimeUsed)
                break
        
            if exitFlag:
                print("exit signal received, exiting now....")
                exitFlag = False
                break
            voltage = voltage + voltage_step
            
        for i in range(0,len(padList)):
            if MeasureMode == 'CV':
                os.system("echo CONFIG: frequency:{}, ac_voltage:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(frequency,ac_voltage,delay,precision,timeUtil(),fileList[i]))
            if MeasureMode == 'IV':
                os.system("echo CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(voltage_step,guardRingGround,delay,precision,timeUtil(),fileList[i]))
            
    except KeyboardInterrupt as exc:
        print("Warning: ",MeasureMode," Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown()



def MeasureIV(voltage_max=500,filename="ivlist.csv"):
    global voltage_step, precision, voltage_breakdown, doStable, useChannelB, exitFlag
    #global SMU#,SMU_LV
    #SMU_LV.write("smua.source.output=smua.OUTPUT_ON")
    
    print(SMU_LV.write(":OUTP1 ON"))
    if useChannelB:
        #print(SMU_LV.write(":SOURce2:GCONnect ON")) 
        print(SMU_LV.write(":OUTP2 ON"))
        #SMU_LV.write("smub.source.output=smub.OUTPUT_ON")    
    #SMU.compliance_current = 5e-6 # with out HV modual 
    SMU.compliance_current =current_compliance # with out HV modual 
    #SMU.compliance_current = 100e-6 # with out HV modual 
    precision   = 1e-9 #1nA abs precision
    #voltage_step    = 1.0
    voltage_breakdown  = -1 
    #doStable=0
    try:    
        with open(filename,"w") as file:
            file.write("CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{}\n".format(voltage_step,guardRingGround,delay,precision,timeUtil()))
            file.write("V\t,I\t,I_GR\n")
            pass_threshold_count = 0
            voltage = 0.0
            voltage_step_tmp = voltage_step

            while voltage <= voltage_max:
                
            # for voltage in np.arange(0,voltage_max,voltage_step):
                voltage=round(voltage,3)
                if voltage < voltage_min:
                    if voltage < int(voltage_min):
                        if (voltage - int(voltage)) > 0:
                            voltage = voltage + voltage_step
                            continue
                        setVoltage(voltage)
                        sleep(0.1)
                        voltage = voltage + voltage_step
                        continue
                    setVoltage(voltage)
                    sleep(voltage_step*0.1)
                    voltage = voltage + voltage_step
                    continue
                # if voltage >= 290:
                #     voltage_step = 0.5

                current_result = MeasureOnce(voltage,"I",delay)

                file.write("{}, {}, {}, {}\n".format(-voltage,current_result[0],current_result[1],current_result[2]))
                file.flush()
                print("\n-------------V=",voltage,"I=",current_result[0],"I_P1=",current_result[1],"I_P2=",current_result[2],timeUtil())
                if max(abs(current_result[1]), abs(current_result[0]), abs(current_result[2])) >= current_compliance * 0.9:
                    
                    if voltage_breakdown < 0 :
                        voltage_breakdown = voltage
                    pass_threshold_count=pass_threshold_count+1
                voltage = voltage + voltage_step    
                if pass_threshold_count>=3:
                    break
                
                if exitFlag:
                    print("exit signal received, exiting now....")
                    exitFlag = False
                    break
            
    except KeyboardInterrupt as exc:
        print("Warning: IV Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown()
        

def MeasureIV5x5VBD(voltage_max=500,filename="ivlist.csv"):
    global voltage_step, precision, voltage_breakdown, doStable, useChannelB, exitFlag, voltage_fin, continueFlag
    
    print(SMU_LV.write(":OUTP1 ON"))
    if useChannelB:
        print(SMU_LV.write(":OUTP2 ON"))
    SMU.compliance_current =current_compliance # with out HV modual 
    precision   = 1e-9 #1nA abs precision
    voltage_breakdown  = -1 

    padList = []
    CHAList = [5,14,19,23,24]
    fileList = []
    swCmdList = []
    Iout = 0
    # for m in CHAList:
    for m in range(1,26):
        print('mode:', m)
       
        PUT = "CH" + str(m)
        print('mode:', m, ' PUT:', PUT)
        
        pad = str(m)
        
        pad_raw = pad
        if len(pad) == 1: 
            pad = "0" + pad  
        newfile = filename[0:filename.rfind('/')+1] + pad + ".csv"    
        print('measuring pad:', PUT, 'save as:', newfile)
        
        if os.path.exists(newfile):
            if False : # doOverwrite
                print("file:",newfile,"already exists, overwrite is enabled, old file is deleted......")
                cmd = "del "+newfile.replace("/","\\")
                os.system(cmd)
                pass
            else:
                print("file:",newfile,"already exists, overwrite not enabled, continue......")
                continue
        ch = int(pad)
        # GNDStr = "1111"
        
        # GNDStr = GNDStr[:Iout] + "0" + GNDStr[Iout+1:]
        
        # #swCmd = r"python swboard.py "+str(m)+" 4 4 " + GNDStr
        # if SwitchBoardVersion == "V2":
        #     swCmd = r"python swboard.py "+str(m)+" "+str(Iout)+" 4 " + GNDStr
        # elif SwitchBoardVersion =="V3":
        #     swCmd = r"python swboard.py "+ str(m)
        # else:
        #     print("Warning, Invalid Switch Board Version :", SwitchBoardVersion, "Switch command is empty")

        padList.append(pad)
        fileList.append(newfile)
        # swCmdList.append(swCmd)






    try:
        
        tWholeTotalBefore = time.time()
        nPointCount = -1
        rdelay = delay
        voltage = 0.0
        for voltage in np.arange(0,voltage_fin, 3.0):
                voltage=round(voltage,3)
                setVoltage(voltage)
                sleep(1.0)
        setVoltage(voltage_fin)
        sleep(1.0)

        for i in range(0,len(padList)):
            winsound.Beep(2000,500)
            firstFlag = True
            pass_threshold_count = 0
            continueFlag = False
            tTotalBefore = time.time()
            # print("exec cmd:",swCmdList[i])
            # tSwitchBefore = time.time()
            # os.system(swCmdList[i])
            # tSwitchEnd = time.time()
            # print("switch executed, takes ",tSwitchEnd - tSwitchBefore," seconds")
            
            for voltage in np.arange(voltage_fin,voltage_min,1.0):
                voltage=round(voltage,3)
                setVoltage(voltage)
                sleep(0.1)
            setVoltage(voltage_min)
            voltage_lv_max=voltage_max-voltage_min
            for voltage in np.arange(0,voltage_lv_max,voltage_step):
                voltage = round(voltage,3)
                setVoltage(voltage,"chA")

                result=[]
                if firstFlag:
                    os.system("echo CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{} >> {}".format(voltage_step,guardRingGround,delay,precision,timeUtil(),fileList[i]))
                    os.system("echo V\t,I\t,I_P1\t,I_P2 >> {}".format(fileList[i]))


                tMeasBefore = time.time()
                result = MeasureOnce(voltage_min,"I",delay)
                tMeasEnd = time.time()
                pad_voltage = voltage_min + voltage
                print("measured pad:", padList[i], "at voltage ", pad_voltage," takes ",tMeasEnd - tMeasBefore," seconds")

                tSaveBefore = time.time()
                os.system("echo {}, {}, {}, {} >> {}".format(-pad_voltage,result[0],result[1],result[2],fileList[i]))
                tSaveEnd = time.time()
                print("save result to file:",fileList[i], "at voltage ", pad_voltage," takes ",tSaveEnd - tSaveBefore," seconds")
                tTotalEnd = time.time()
                print("\n-------------V=",pad_voltage,"I=",result[0],"I_P1=",result[1],"I_P2=",result[2],timeUtil()," ...total takes",tTotalEnd - tTotalBefore," seconds")

                measureSpeed = round((tTotalEnd - tTotalBefore)/(1.0*voltage_step),3)

                print("Finished voltage---",pad_voltage,"result",result,"exitFlag:",exitFlag,"firstFlag:",firstFlag)   

                firstFlag = False

                if max(abs(result[1]), abs(result[0]), abs(result[2])) >= current_compliance * 0.9 or abs(result[1]) >= 1e-8:
                    
                    if voltage_breakdown < 0 :
                        voltage_breakdown = voltage
                    pass_threshold_count=pass_threshold_count+1
                    
                if pass_threshold_count>=3:
                    break
                
                if exitFlag:
                    print("exit signal received, exiting now....")
                    exitFlag = False
                    break
            for voltage in np.arange(voltage_now_chA, 1.0,-1.0):
                setVoltage(voltage,"chA")
                sleep(0.2)
            setVoltage(0,"chA")
            for voltage in np.arange(voltage_now, voltage_fin,-1.0):  # [max,min)
                setVoltage(voltage)
                sleep(0.2)
            setVoltage(voltage_fin)
            winsound.Beep(2000,2000)
            print("Waiting for charging channel....")
            while(1):
                if continueFlag == True:
                    break
                sleep(1.0)
            print("Continue to measure next channel....")
            sleep(3.0)
            if exitFlag:
                    print("exit signal received, exiting now....")
                    exitFlag = False
                    break
            

            
    except KeyboardInterrupt as exc:
        print("Warning: IV Measure KeyboardInterrupt:",type(exc),exc.args)
    except Exception as exc:
        print("Exception in app.exec()",type(exc))
        print(exc.args)
    finally:
        shutdown5x5VBD()
        

def MeasureIV_AllBias(voltage_max=500,filename="ivlist.csv"):
    global voltage_step, precision, voltage_breakdown, doStable, useChannelB
    global voltage_now_chA,voltage_now_chB
    global SMU#,SMU_LV
    recFile = filename
    rawFile = filename
    #SMU_LV.write("smua.source.output=smua.OUTPUT_ON")
    if useChannelB:
        SMU_LV.write("smub.source.output=smub.OUTPUT_ON")    
    SMU.compliance_current = current_compliance
    precision   = 1e-9 #1nA abs precision
    voltage_step    = 1.0
    voltage_breakdown  = -1 
    doStable=0
    #current_compliance * 0.9 = 1e-8
    voltage_space = 10
    voltage_lv_max = 50
    voltage_hv_keep = -1
    try:    
        with open(filename,"w") as file:
            file.write("CONFIG: voltage_step:{}, GRground:{}, delay:{}, precision:{}, timestamp:{}\n".format(voltage_step,guardRingGround,delay,precision,timeUtil()))
            file.write("V,\tI_total,\tI_chA,\tI_chB\n")
            pass_threshold_count = 0
            
            for voltage in np.arange(0,voltage_max,voltage_step):
                voltage=round(voltage,3)
                current_result = MeasureOnce(voltage,"I")

                file.write("{}, {}, {}, {}\n".format(-voltage,current_result[0],current_result[1],current_result[2]))
                file.flush()
                print("\n-------------V=",voltage,"I=",current_result[0],"I_P1=",current_result[1],"I_P2=",current_result[2],timeUtil())
                if max(abs(current_result[1]), abs(current_result[0]), abs(current_result[2])) >= current_compliance * 0.9 and voltage_now > 5:
                    
                    if voltage_breakdown < 0 :
                        voltage_breakdown = voltage
                    pass_threshold_count=pass_threshold_count+1
                    
                if pass_threshold_count>=6:
                    break
            
            voltage_hv_keep = voltage_breakdown - voltage_space

            for voltage in np.arange(voltage_now, voltage_hv_keep,-1.0):  # [max,min)
                print("RAMPDOWN_HV FOR SPACE-------------v=",voltage,"---voltage_now=",voltage_now)
                setVoltage(voltage)
                sleep(0.1)



            voltage_breakdown = -1
            pass_threshold_count = 0
            # Measure ChannelA------------------
            for voltage in np.arange( 0, voltage_lv_max, voltage_step):
                voltage = round(voltage,3)
                setVoltage(voltage,"chA")
                current_result = MeasureOnce(voltage_hv_keep,"I")
                pad_voltage = voltage_hv_keep + voltage 
                file.write("{}, {}, {}, {}\n".format(-pad_voltage,current_result[0],current_result[1],current_result[2]))
                file.flush()
                print("\nMeasure-Pad-A---------V=",pad_voltage,"I=",current_result[0],"I_P1=",current_result[1],"I_P2=",current_result[2],timeUtil())
                if max(abs(current_result[1]), abs(current_result[0]), abs(current_result[2])) >= current_compliance * 0.9  and voltage_now > 5:
                    
                    if voltage_breakdown < 0 :
                        voltage_breakdown = pad_voltage
                    pass_threshold_count=pass_threshold_count+1
                    
                if pass_threshold_count>=6:
                    break
                if voltage > 80:
                    break
                    
                   
            for voltage in np.arange(voltage_now_chA, 0,-1.0):  # [max,min)
                print("RAMPDOWN_ChA FOR SPACE-------------v=",voltage,"---voltage_now=",voltage_now_chA)
                setVoltage(voltage,"chA")
                sleep(0.1)
            setVoltage(0,"chA")
            
            
            voltage_breakdown = -1
            pass_threshold_count = 0
            # Measure ChannelB=================
            for voltage in np.arange( 0, voltage_lv_max, voltage_step):
                voltage = round(voltage,3)
                setVoltage(voltage,"chB")
                current_result = MeasureOnce(voltage_hv_keep,"I")
                pad_voltage = voltage_hv_keep + voltage 
                file.write("{}, {}, {}, {}\n".format(-pad_voltage,current_result[0],current_result[1],current_result[2]))
                file.flush()
                print("\nMeasure-Pad-B---------V=",pad_voltage,"I=",current_result[0],"I_P1=",current_result[1],"I_P2=",current_result[2],timeUtil())
                if max(abs(current_result[1]), abs(current_result[0]), abs(current_result[2])) >= current_compliance * 0.9  and voltage_now > 5:
                    
                    if voltage_breakdown < 0 :
                        voltage_breakdown = pad_voltage
                    pass_threshold_count=pass_threshold_count+1
                    
                if pass_threshold_count>=6:
                    break
                if voltage > 80:
                    break    
                   
            for voltage in np.arange(voltage_now_chB, 0,-1.0):  # [max,min)
                print("RAMPDOWN_ChB FOR SPACE-------------v=",voltage,"---voltage_now=",voltage_now_chB)
                setVoltage(voltage,"chB")
                sleep(0.1)

            setVoltage(0,"chB")
            
                        
    except KeyboardInterrupt as exc:
        print("Warning: IV Measure KeyboardInterrupt:",type(exc),exc.args)
    finally:
        shutdown()

        
        
def shutdown():
    """ Ramp down at 1V step, 100 ms/V rate"""
    global voltage_now, current_now, useChannelB

    winsound.Beep(2000,500)
    print("Shutdown---voltage_now=",voltage_now)
    for voltage in np.arange(voltage_now, -0.2,-12.0):  # [max,min)
#    for voltage in np.arange(voltage_now, -0.2,-1.0):  # [max,min)
        print("RAMPDOWN-------------v=",voltage,"---voltage_now=",voltage_now)
        setVoltage(voltage)
        
        #if withSMU:
            #current_now=SMU.current
        sleep(0.1)
    setVoltage(0.0)
    if withLCR:
        LCR.shutdown()
    if withSMU:
        SMU.shutdown()
   
    if withSMULV:
        
        
        SMU_LV.write(":OUTP1 OFF")
        #SMU_LV.write("smua.source.output=smua.OUTPUT_OFF")
        
        if useChannelB:
            SMU_LV.write(":OUTP2 OFF")       
            #SMU_LV.write("smub.source.output=smub.OUTPUT_OFF")
        # SMU_LV.write("beeper.beep(3, 1400)")
    winsound.Beep(2000,1000)


def shutdown5x5VBD():
    """ Ramp down at 1V step, 100 ms/V rate"""
    global voltage_now, current_now, useChannelB

    winsound.Beep(2000,500)
    print("Shutdown---voltage_now=",voltage_now)
    
    for voltage in np.arange(voltage_now_chA, 1.0,-1.0):
        SMU_LV.write(":SOUR1:VOLT -"+str(voltage))
        sleep(0.2)
    SMU_LV.write(":SOUR1:VOLT 0")


    for voltage in np.arange(voltage_now, 1.0,-2.0):  # [max,min)
#    for voltage in np.arange(voltage_now, -0.2,-1.0):  # [max,min)
        print("RAMPDOWN-------------v=",voltage,"---voltage_now=",voltage_now)
        setVoltage(voltage)
        
        #if withSMU:
            #current_now=SMU.current
        sleep(0.1)
    setVoltage(0)
   
    if withSMULV:
        
        
        SMU_LV.write(":OUTP1 OFF")
        #SMU_LV.write("smua.source.output=smua.OUTPUT_OFF")
        
        if useChannelB:
            SMU_LV.write(":OUTP2 OFF")       
            #SMU_LV.write("smub.source.output=smub.OUTPUT_OFF")
        # SMU_LV.write("beeper.beep(3, 1400)")
    winsound.Beep(2000,1000)

 