#-*-coding:utf-8-*-
# Developed by USTC
from __future__ import print_function

# Driver programme to control 25-channel SwitchBoard for 5x5 probecard
# Author: Yang Xiao 
# Dependency: 
#      python3, hidapi (https://github.com/trezor/cython-hidapi) 
# Install: 
#   $ sudo pip3 install hidapi

import hid
import string 
import sys
import time

# GND: 0xBC 
# Measure: 0xAA or else 
IoutA_GND = 0xBC
IoutB_GND = 0xAA
IoutC_GND = 0xBC
IoutD_GND = 0xBC

# Vid = 0x0483 #VendorID
# Pid = 0x5750 #ProductID

Vid = 0x0483 #VendorID
Pid = 0x5750 #ProductID

def set(Iout = 0x00, Vout = 0x04, Gain = 0x04, GNDStr = "1111"): #default:  Vout, Gain off
    global IoutA_GND,IoutB_GND,IoutC_GND,IoutD_GND
    global Vid, Pid
    
    IoutA_GND = ( 0xBC if (GNDStr[0] == '1') else 0xAA )
    IoutB_GND = ( 0xBC if (GNDStr[1] == '1') else 0xAA )
    IoutC_GND = ( 0xBC if (GNDStr[2] == '1') else 0xAA )
    IoutD_GND = ( 0xBC if (GNDStr[3] == '1') else 0xAA )
    
    # enumerate USB devices 
    # for d in hid.enumerate():
    #     keys = list(d.keys())
    #     keys.sort()
    #     for key in keys:
    #         print("%s : %s" % (key, d[key]))
    #     print()
        
    
    # try opening a device, then perform write and read
    try:
        # print("Opening the device")
        h = hid.device(Vid, Pid)
        h.open(Vid, Pid) # TREZOR VendorID/ProductID
        
       #print("Manufacturer: %s" % h.get_manufacturer_string())
       #print("Product: %s" % h.get_product_string())
       # print("Serial No: %s" % h.get_serial_number_string())

        # enable non-blocking mode
        h.set_nonblocking(1)

        # write some data to the device
        #--print("Write the data")
        #h.write([0x00, 0x01, 0x02, 0x03] + [0x00] * 12)
        cmd = [0xBB,0xAA] + [0x7E,0x5B,0x32,0x6C] + [Iout] * 3 + [Vout] * 3 + [Gain] * 3 + [IoutA_GND, IoutB_GND, IoutC_GND, IoutD_GND] + [0x8F, 0xBD, 0xA9, 0xB0]
        #cmd = [0xAA,0xAA,0x7E,0x5B,0x32,0x6C,0x01,0x01,0x01,0x04,0x04,0x04,0x04,0x04,0x04,0xBC,0xAA,0xBC,0xBC,0x8F,0xBD,0xA9,0xB0]
        # print ("cmd: ") 
        # for bit in cmd:
        #     print (hex(bit))
        # print ((cmd))
        # print (".")
        #h.write([0xAA] + [0x7E,0x5B,0x32,0x6C] + [Iout] * 3 + [Vout] * 3 + [Gain] * 3 + [IoutA_GND, IoutB_GND, IoutC_GND, IoutD_GND] + [0x8F, 0xBD, 0xA9, 0xB0])
        tWriteStart = time.time()
        h.write(cmd)
        tWriteEnd = time.time()
        print("SwitchUSB cmd write executed ..... takes",tWriteEnd - tWriteStart," seconds.");
        #print (h)
        # wait
        time.sleep(0.01) # 10ms wait

        # read back the answer
        
        #--print("Read the data")
        print(" >>output: ",end='')
        while True:
            d = h.read(20)
            if d:
                # for i in d:
                    # print("%02x " % i, end='')
                # print('  ||  ', end='')
                for i in d:
                    if chr(i) in string.printable : 
                        print("%c" % i, end='')
                    else:
                        print("*", end='')
                print("")
            else:
                break
        print("")
        #print("Closing the device")
        h.close()
        return 0
    except IOError as ex:
        print(ex)
        print("Error, device [VendorID]:[ProductID] = %04x:%04x not found, please use 'lsusb' to check connection." % (Vid, Pid)) 
        #You probably don't have the hard coded device. Update the hid.device line")
        #print("in this script with one from the enumeration list output above and try again.")
        return -1
    print("Done")

def getCh(Iout = 0x00):

    IoutCmdList = [
        # IoutA   IoutB  IoutC  IoutD
        ["GND" , "CH13", "GND" , "GND" ], # 00
        ["CH9" , "CH8" , "CH3" , "CH4" ], # 01
        ["CH10", "CH7" , "CH2" , "CH5" ], # 02
        ["GR"  , "CH6" , "CH1" , "CH15"], # 03
        ["CH25", "CH16", "CH11", "CH20"], # 04
        ["CH24", "CH12", "CH21", "CH14"], # 05
        ["CH18", "CH17", "CH22", "CH19"], # 06
        ["GND" , "CH23", "GND" , "GND" ], # 07
        ["GND" , "GND" , "GND" , "GND" ], # 08
        ["GND" , "GND" , "GND" , "GND" ], # 09
    ]
    if Iout > 0x09: return IoutCmdList[0x00]
    return IoutCmdList[Iout]



if __name__ == "__main__":

    Iout = 0x00
    Vout = 0x04
    Gain = 0x04
    GNDStr = "0000"
    
    if len(sys.argv) <= 1:
        print(" Usage: python3 "+sys.argv[0]+" [Iout] [Vout] [Gain] [GNDStr]")
        print(" \n too few arguments...exit")
        exit(0)

    if len(sys.argv) > 1:
        Iout = int(sys.argv[1])
        #print("set Iout command: 0x%02x" % Iout) 
        print("set Iout status: ", end = '') 
        #print(getCh(Iout),end = '')
        if len(sys.argv) == 2:
            Vout = Iout
            Gain = Iout
    if len(sys.argv) > 2:
        #Vout = int(sys.argv[2])
        Iout = int(sys.argv[1]) *32 + int(sys.argv[2])
        if len(sys.argv) == 3:
            Vout = Iout
            Gain = Iout
        #print("set Vout command: 0x%02x" % Vout) 

    if len(sys.argv) > 3:
        Gain = int(sys.argv[3])
        #print("set Gain command: 0x%02x" % Gain) 
    if len(sys.argv) > 4:
        GNDStr = str(sys.argv[4])
        #print("set GNDStr command: %s" % GNDStr) 
    
    set(Iout, Vout, Gain, GNDStr)
