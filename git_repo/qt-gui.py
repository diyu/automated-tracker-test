# -*- coding: utf-8 -*-
# Developed by USTC
from PyQt5 import QtGui, QtCore 
from PyQt5.QtCore import QThread,QTimer,Qt
from PyQt5.QtWidgets import QMessageBox, QCheckBox,QGroupBox,QFrame
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import sys
import math
import tail
import os
#import commands
import time
from time import sleep
import threading
#import measure_fake1 as measure 
import measure as measure


IoutCmdList = [
    # IoutA   IoutB  IoutC  IoutD
    ["GND" , "CH13", "GND" , "GND" ], # 00
    ["CH9" , "CH8" , "CH3" , "CH4" ], # 01
    ["CH10", "CH7" , "CH2" , "CH5" ], # 02
    ["GR"  , "CH6" , "CH1" , "CH15"], # 03
    ["CH25", "CH16", "CH11", "CH20"], # 04
    ["CH24", "CH12", "CH21", "CH14"], # 05
    ["CH18", "CH17", "CH22", "CH19"], # 06
    ["GND" , "CH23", "GND" , "GND" ], # 07
    ["GND" , "GND" , "GND" , "GND" ], # 08
    ["GND" , "GND" , "GND" , "GND" ], # 09
]


PositionMap = [
                '1x1_GRW20_01',
                '1x1_GRW40_01',
                '1x1_GRW80_01',
                '1x1_GRW100_01',

                '1x1_PSSP22-FPL3_01',
                '1x1_PSSP35_01',
                '1x1_PSSP30_01',
                '1x1_PSSP22_01',

                '1x1_PSSP30-FPL3_01',
                '1x1_PSSP30-FPL8_01',
                '1x1_PSSP30-FPL10_01',
                '1x1_FPC-EXT_01',

                '1x1_NPE-N_01',
                '1x1_PSW8_01',
                '1x1_PSW7_01',
                '1x1_PSW5_01',
                
                '1x1_FP-N_01',
                '1x1_FPL3_01',
                '1x1_FPL8_01',
                '1x1_FPL10_01',

                '1x1_FGR-FP_01',
                '1x1_LFGR_01',
                '1x1_GRFP-OL13_01',
                '1x1_GRFP-N_01',
    
                '1x1_NFGR0_01',
                '1x1_NFGR2_01',
                '1x1_NFGR3_01',
                '1x1_NPASS_01',

                '1x1_GRW100_02',
                '1x1_GRW80_02',
                '1x1_GRW40_02',
                '1x1_GRW20_02',
    
                '1x1_PSSP22_02',
                '1x1_PSSP30_02',
                '1x1_PSSP35_02',
                '1x1_PSSP22-FPL3_02',

                '1x1_FPC-EXT_02',
                '1x1_PSSP30-FPL10_02',
                '1x1_PSSP30-FPL8_02',
                '1x1_PSSP30-FPL3_02',
    
                '1x1_PSW5_02',
                '1x1_PSW7_02',
                '1x1_PSW8_02',
                '1x1_NPE-N_02',

                '1x1_FPL10_02',
                '1x1_FPL8_02',
                '1x1_FPL3_02',
                '1x1_FP-N_02',
    
                '1x1_GRFP-N_02',
                '1x1_GRFP-OL13_02',
                '1x1_LFGR_02',
                '1x1_FGR-FP_02',

                '1x1_SR70_01',
                '1x1_SR50_01',
                '1x1_SR30_01',
                '1x1_SR20_01',

                '1x1_PSSP35-FPL10_01',
                '1x1_PSSP40-FPL10_01',
                '1x1_PSSP40-SSAFE_01',
                '1x1_PSSP40-SSAFE_02',

                '1x1_NM_01',
                '1x1_NH_01',
                '1x1_NFM_01',
                '1x1_GRNPL-N_01',
    
                '1x1_MPASS_01',
                '1x1_MMESH_01',
                '1x1_MRING_01',
                '1x1_PIN_01',

                '1x1_PSSP30-PIN_01',
                '1x1_PSSP22-PIN_01',
                '1x1_BLRD-S_01',
                '1x1_BLRD_01',
    
                '1x1_PSSP40-SSAFE-PIN_01',
                '1x1_BLRD-PIN_01',
                '1x1_NPSTOP_01',
                '1x1_NPERI_01',

                '1x1_BL_04',
                '1x1_BL_03',
                '1x1_BL_02',
                '1x1_BL_01',

                '1x1_BL_05',

                '1x1_LARGE_01',
    
                '2x2_BL_01_01',
                '2x2_BL_02_01',
                '2x2_BL_03_01',

                '2x2_IP9_01_01',
                '2x2_IP5_01_01',
                '2x2_IP3_01_01',

                '2x2_IP3_02_01',
                '2x2_IP5_02_01',
                '2x2_IP9_02_01',

                '2x2_IPBND_01_01',
                '2x2_SE2_01_01',
                '2x2_SE1_01_01',

                '2x2_SE5_01_01',
                '2x2_SE2_02_01',
                '2x2_IPBND-PIN_01_01',

                '2x2_SE5-NFGR3S_01_01',
                '2x2_SE5-NFGR1S_01_01',
                '2x2_SE5-NFGR2_01_01',

                '2x2_SE5-NFGR3_01_01',
                '2x2_SE5-NFGR2S_01_01',
                '2x2_SE5-NFGR4S_01_01',
                
                '2x2_NM_01_01',
                '2x2_PIN_01_01',
                '2x2_LGPIN_01_01',
    
                '2x2_MMESH_01_01',
                '2x2_MRING_01_01',
                '1x1_LARGE_01',
    
                '15x15_BL-UBM_01_01',
    
                '5x5_IP7_01_01',
                '5x5_IP5_01_01',
    
                '5x5_IP7-NM_01_01',
                '5x5_IP9_01_01',
                
                '1x1_AFP_01',
                '1x1_PIN_02'

]



filename=""
mode="IV"#IV or CV
voltageMax=300
useChannelB = True
doStable = False 
#enableParallel = True
enableParallel = False
enable15x15 = False
enable5x5VBD = False
currentCh1 = 1e-12
currentCh2 = 1e-12
timeZero  = -1.0

def info(msg):
    print("[INFO] ",msg)
def error(msg):
    print("[ERROR] ",msg)
def warn(msg):
    print("[WARNNING] ",msg)
def debug(msg):
    print("[DEBUG] ",msg)
def redFont(str):
    return '<font size="6" color="red">  '+str+'  </font>'
def greenFont(str):
    return '<font size="6" color="green">  '+str+'  </font>'
def coloredFont(str,color="red"):
    return '<font size="6" color="'+color+'">  '+str+'  </font>'



def print_to_string(*args, **kwargs):
    output = io.StringIO()
    print(*args, file=output, **kwargs)
    contents = output.getvalue()
    output.close()
    return contents
    
    
def startGraph():
    """Start to Graphing"""
    global th1,timer,timeZero,timerMon
    global voltageMax,filename
    filename = t1.text()
    if 'th1' in globals():
        stopGraph()

    th1= myThread(filename)
    try:
        print(filename)
        th1.start()
        sleep(1)
        timer.start(500)
        if timeZero < 0:
            timeZero = time.time()
            
    except Exception as exc:
        error("Exception:",type(exc),exc.args)
    else: 
        print("Thread Started Successfully!")    
    lmode.setText("Graph Status: Graphing")
    if not timerMon.isActive():
        timerMon.start(200)



def stopGraph():
    global th1,t,timer
    if 'th1' in globals() and 't' in globals():
        t.exitFlag=True
        sleep(1)
        lmode.setText("Graph Status: Stopped")  
        del th1        
    else:
        lmode.setText("Graph Status: Nothing to Stop, no graph thread find")
    
    if timerMon.isActive():
       timerMon.stop()    

def _init():
    global timerMon,x5,y5,x6,y6,x7,y7
    
    info("measure initializing.....")
    if 'voltage' not in dir(measure):        
        info("initializing instruments.....")
        measure.init()
        info("initializing instruments.....done")

    _updateConfig()
    y5 = []
    x5 = []
    y6 = []
    x6 = []
    y7 = []
    x7 = []    
    if not timerMon.isActive():
        timerMon.start(200)
    lstatus.setText("Meausure Status: Ready")
    lmode.setText("Graph Status: Ready")
    info("measure initialized....\n")
    timeZero = time.time()
    
def LCRCorrOpen():
    measure.LCRCorr("open")

def LCRCorrShort():
    measure.LCRCorr("short")

def _shutdown():
    global thMeas
    info("measure shutdown......")
    
    if 'thMeas' in globals():
        print("aborting current measure thread.....")
        _measureAbort()
        del thMeas
        lstatus.setText("Meausure Status: Measure Aborted")
    else:
        lstatus.setText("Meausure Status: Nothing to Stop, no measure thread find")
    lstatus.setText("Meausure Status: Shutting down...")
    measure.shutdown()
    lstatus.setText("Meausure Status: Shutdown Finished")    
    print("measure shutdown......done\n")
    
def updateFileNameHist(txt):
    hist = '.log_filename'    
    f= open(hist,"r")
    cont = f.read()
    cont_sp = cont.split();
    if cont_sp[-1] == txt:
        print("contains ",txt,"already... not save")
    else:
        os.system("echo {} >> {}".format(txt,hist))

def fetchFileNameHist():
    hist = '.log_filename'    
    f= open(hist,"r")
    cont = f.read()
    cont_sp = cont.split();
    return cont_sp[-1]
    
def _updateConfig():
    global voltageMax,filename
    _setmodeCb()
    voltageMax=float(lvoltage.text())
    filename = t1.text()
    updateFileNameHist(filename)
    
    measure.useChannelB = useChannelB
    measure.voltage_min = float(lvoltage_min.text())
    measure.frequency = 1000.0 * float(lfrequency.text())
    measure.step = float(lstep.text())
    measure.voltage_step = float(lstep.text())
    measure.delay = float(ldelay.text())
    measure.measure_average_times=1
    measure.doStable = doStable
    measure.precision = float(lprecision.text())
    measure.current_compliance = float(lcompliance.text())

    
    measure.voltage_fin = float(lvoltage_fin.text())


    lconfig.setText("Configure: voltageMax:"+str(voltageMax)+" mode:"+mode+" delay:"+str(measure.delay)+" step:"+str(measure.step))
    linfo.setText("File:"+coloredFont(filename,"blue"))
    print("setting have been update.....")


def _measure():
    global thMeas
    print("\nmeasure start......")
    if 'thMeas' in globals():
        if not thMeas.dead:
            print("Warning: measure is ongoing... please stop it first!")       
            return 0
        else:
            print("Deleting previous dead measure thread.....")   
            del thMeas
    _init()
    #_updateConfig()   
    print("after _updateConfig")

    if os.path.exists(filename):
        reply = QMessageBox.question(None, 'File Overwrite Confirmation', 'FIile:'+filename+' Exists ! \n Are you sure to overwrite it?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            pass
        else:
            return
    path = filename[0:filename.rfind('/')]
    if not os.path.exists(path):
        reply = QMessageBox.question(None, 'Directory Not Found', 'Directory:'+path+' not found, \n Are you want to create a new one?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            print("excuting:make dir.: "+path)
            os.makedirs(path) #power shell style
        else:
            return


    print("before creating measure thread")
    thMeas = measThread(filename,voltageMax,mode)
    print("after creating measure thread")
    try:
        print("before start measure thread")
        thMeas.start()
        print("after start measure thread")
    except Exception as exc:
        print("Warning:Exception:",type(exc),exc.args)
        lstatus.setText("Meausure Status: Exception")
    else: 
        print("Thread Started Successfully!")
    lstatus.setText("Meausure Status: "+coloredFont("Measuring","red"))
    sleep(1)
    startGraph()
    print("after started graph")
    

    print("measure start......done")


def _measureAbort():
    print("......measure aborting......")
    measure.exitFlag = True
    i = 0
    while measure.exitFlag and i < 10:
        print("......waiting to abort measure...... i=",i)
        i = i + 1
        sleep(1)
    if not measure.exitFlag:
        print("......measure abort......done")
    else:
        print("......measure abort......failed after 10s wait")

def _Continue():
    print("......measure continue......")
    measure.continueFlag = True


def _setmode(self):
    global mode
    radioButton = win.sender()
    if radioButton.isChecked():
            mode=radioButton.mode
            print("mode changing to ",mode," ......")
    if mode == "CV":
        p2.show()
    else:
        p2.hide()
        
    _updateConfig()
    
def _switch():
    ch = int(lswitch.text())

    GNDStr = ""
    GNDStr = GNDStr + ("1" if(GndACb.checkState() == Qt.Checked) else "0")
    GNDStr = GNDStr + ("1" if(GndBCb.checkState() == Qt.Checked) else "0")
    GNDStr = GNDStr + ("1" if(GndCCb.checkState() == Qt.Checked) else "0")
    GNDStr = GNDStr + ("1" if(GndDCb.checkState() == Qt.Checked) else "0")

    print("Switching to readout mode ", ch," GNDStr:",GNDStr) 
    
    IoutText = cbatch_select.currentText()
    if IoutText == "Iout A" : Iout = 0 ;
    if IoutText == "Iout B" : Iout = 1 ;
    if IoutText == "Iout C" : Iout = 2 ;
    if IoutText == "Iout D" : Iout = 3 ;
    if IoutText == "All" : Iout = 0 ;


    Vout = str(Iout)


    switchChannel(ch, GNDStr, Vout)    

def _ustcReplace():
    filename = t1.text()
    ustcid = ustcid_select.currentText()
    newfile = filename[0:filename.rfind('/')+1] + ustcid + ".csv"
    t1.setText(newfile)
    _measure()

    
def _ustcNext():
    print ("before:",ustcid_select.currentText())
    print ("index",ustcid_select.currentIndex())
    index = ustcid_select.currentIndex()
    print (ustcid_select.count())
    if index + 1 > ustcid_select.count() - 1 :
        index = -1
    ustcid_select.setCurrentIndex(index + 1)
    print ("after:",ustcid_select.currentText())
    print ("after index",ustcid_select.currentIndex())
    print (" ")

def _ustcGo():
    _ustcNext()
    _ustcReplace()





def _dostable(self):
    global doStable
    if dostablebutton.checkState() == Qt.Checked:
        doStable=True
    else:
        doStable=False
    print("doStable=",doStable)
    _updateConfig()

def _setmodeCb():
    global useChannelB
    if radiobuttonCb.checkState() == Qt.Checked:
        useChannelB=True
    else:
        useChannelB=False
    print("useChannelB=",useChannelB)
    
def _setParallelCb():
    global enableParallel
    if radiobuttonParaCb.checkState() == Qt.Checked:
        enableParallel=True
    else:
        enableParallel=False
        
    print("enableParallel=",enableParallel)  

def _set15x15Cb():
    global enable15x15
    if radiobuttonParaCb_15x15.checkState() == Qt.Checked:
        enable15x15=True
    else:
        enable15x15=False
        
    print("enable15x15=",enable15x15)  

def _set5x5VBDCb():
    global enable5x5VBD
    if radiobuttonParaCb_5x5VBD.checkState() == Qt.Checked:
        enable5x5VBD=True
    else:
        enable5x5VBD=False
        
    print("enable5x5VBD=",enable5x5VBD)  


def _setLogy():
    global setLogY
    if LogYCb.checkState() == Qt.Checked:
        setLogY=True
    else:
        setLogY=False
    pi.setLogMode(None,setLogY)
    
    print("setLogY=",enableParallel)      
    
        
def doping_cal():
    global x, y1, y2, C2
    
    epsilon = 11.9 * 8.85e-14
    eunit = 1.602e-19
    A = 0.13 * 0.13

    #smooth_times = 2 
    if len(y1) == 2:
        C2 = [ y1[0] , (( 3.0 * y1[0] + y1[1]) / 4.0 )]
    else:
        C2.append(( y1[-1] + 2.0 * y1[-2] + y1[-3]) / 4.0 )

    doping = ( 2.0 / (epsilon * eunit * A * A))/((C2[-1] - C2[-2])/(x1[-1] - x1[-2]))
    width = A * epsilon * (2 / ( y2[-1] + y2[-2] )) * 1.0e4
    return (width,doping)

def update(txt):
    global curve1, curve2, x, y1, y2, y3, ser, size, buffersize,curveList,currentCh1,currentCh2
    try:
        values=txt.split(',')
        print(values)
        V=-float(values[0])
            
        if mode == "IV":
            #I_pad = -(float(values[1])+float(values[2]))
            #I_gaurd_ring = float(values[2])
            
            # for Y2-Pad Y1-Other4Pad
            I_gaurd_ring = -(float(values[1])+float(values[2]))
            I_pad = float(values[2])
            
            
            Y1 = I_pad
            Y2 = I_gaurd_ring
            
            if useChannelB:
                I_total = float(values[1])                
                I_chA =  float(values[2])
                I_chB =  float(values[3])
                
                Y1 = abs(I_total)
                Y2 = abs(I_chA)
                Y3 = abs(I_chB)
                
                currentCh1 = I_chA
                currentCh2 = I_chB

                
        if mode == "CV":
            Cp = float(values[1])
            Rp = float(values[2])
            Y1 = 1.0/(Cp*Cp)
            Y2 = Rp
            Y3 = Cp
  
            
            
    except Exception as exc:
            print("Warning: Unable to process line:",txt,type(exc),exc.args)
    else:
        if mode == "IV":
            x1.append(V)
            y1.append(Y1)
            y2.append(Y2)
        
            curve1.setData(x1,y1)
            curve2.setData(x1,y2)
            if useChannelB:
                y3.append(Y3)
                curve3.setData(x1,y3)
                pass
        if mode == "CV":
            x1.append(V)
            y1.append(Y1)
            y2.append(Y3)
            curve1.setData(x1,y1)
            if len(y1) > 1:
                print("1")
                dopdata = doping_cal()
                x2.append(dopdata[0])
                y4.append(dopdata[1])
                curve4.setData(x1,y4)
                print("2")
            else:
                y4.append(0.0)

def updateMon():
    global curve5, x5, y5,curve6, x6, y6,curve7, x7, y7
    if mode == "IV":
        pass
    #if mode == "CV":
        

    for xi in measure.monStableX:
        x6.append(xi - timeZero)
    measure.monStableX = []
    for yi in measure.monStableY:
        y6.append(abs(yi))
    measure.monStableY = []
    for xi in measure.monBufferX:
        x5.append(xi - timeZero)
    measure.monBufferX = []
    for yi in measure.monBufferY:
        y5.append(yi)
    measure.monBufferY = []        
    for xi in measure.monHVX:
        x7.append(xi - timeZero)
    measure.monHVX = []
    for yi in measure.monHVY:
        y7.append(yi)
    measure.monHVY = []
        
    if len(x5) == len(y5) and len(x6) == len(y6) and len(x7) == len(y7):   
        curve5.setData(x5,y5)
        curve6.setData(x6,y6)
        curve7.setData(x7,y7)
    else:
        error("length of x5:"+str(x5)+" and y5"+str(y5)+" did not consist")        


def update_plt():
    p1.hide()
    p1.show()
    monStr = ""
    monStr = monStr  +  "Current: HV:"+redFont(str(measure.voltage_now))+" V,  "
#     monStr = monStr  +  "ChA:"+str(measure.voltage_now_chA)+" V,  "
#     monStr = monStr  +  "ChB:"+str(measure.voltage_now_chB)+" V,  "
    monStr = monStr  +  "I_total:"+redFont(str(round(measure.current_now * 1e9, 4)))+" nA"
    if mode == "IV":
        monStr = monStr  +  "( I_chA:"+redFont(str(round(currentCh1 * 1e9, 4)))+" nA"  +  ", I_chB:"+redFont(str(round(currentCh2 * 1e9, 4)))+" nA),"
    if mode == "CV":
        monStr = monStr  +  "( Cp:"+redFont(str(round(measure.Cp_now * 1e12, 4)))+" pF"  +  ", Rp:"+redFont(str(round(measure.Rp_now * 1e-3, 4)))+" k*Ohm),"
    monStr = monStr  + "MeasSpeed:"+greenFont(str(round(measure.measureSpeed,3)))+" sec/V," 
    if measure.voltage_breakdown > 0:
        monStr = monStr + "  VBD:" + redFont(str(measure.voltage_breakdown))
        monStr = monStr + "\n Total takes:" + redFont(measure.totalTimeUsed)
    measure.monHVY.append(measure.voltage_now)
    measure.monHVX.append(time.time())
    lmonitor.setText(monStr)

def monitorFile(threadname,filename):    
    global threadName,y1,y2,y3,x1,t,x2,y4 
    y1=[]
    y2=[]
    y3=[]
    x1=[]
    x2=[]
    y4=[]
    
    print('monitorFile start')

    threadName=threadname
    t = tail.Tail(filename)
    t.register_callback(update)
    t.follow(s=0.4)
    print('monitorFile done')

def switchChannel(ch,GNDStr = "0000", Vout = "0"):
    #global GNDStr
    # if ch > 9 :
    #     print("error ch larger than 9, ch:",ch)
    #     return -1
        
    #Vout = "0"
    #Vout = str(Iout)
    
    print("swith to board mode :",ch," "+Vout+" 4 ",GNDStr)
    lbswitch_conf.setText("Sw. Conf: "+ str(ch)+" "+Vout+" 4 " + GNDStr)
    
    if EnSwitchCb.checkState() == Qt.Checked: 
        print("executing the swboard.py script...")
        #rtn = os.system(r"python swboard.py "+str(ch)+" "+Vout+" 4 " + GNDStr)
        rtn = os.system(r"python swboard.py "+str(ch))
        print(rtn)
    else:
        print("warning! the switch is not enabled, swboard.py script would not be executed...")
        
    lbswitch_status.setText("Sw. Status: "+str(ch))
    
def uploadData():
    global filename
    uploadEnable = (EnUploadCb.checkState() == Qt.Checked) 

    print("uploading the data.....")
    path = filename[0:filename.rfind('/')]
    # cmd = "scp -r -P 5822 " + path + " xyang@210.45.72.167:/home/xyang/html/HGTD/7.SensorProbe/Plot/labprob/Data/"+path[0:path.rfind('/')+1]
    #cmd = "scp -r -P 5822 " + path + " xyang@202.141.161.69:/home/xyang/html/HGTD/7.SensorProbe/Plot/labprob/Data/"+path[0:path.rfind('/')+1]
    print("cmd: "+ cmd)
    if uploadEnable:
        print("uplode command executing.......")
        os.system(cmd)
    else:
        print("upload is not enabled... nothing would be upload")

def _saveGraph():
    exporter = pg.exporters.ImageExporter(pi)
    #exporter.parameters()['width'] = int(100)
    savePath = "Plots/"+(filename.replace('/','-')).replace('.csv','.png')
    print("save path:",savePath)
    exporter.export(savePath)
    
def _runBatch():
    global filename, thMeasBatch
    thMeasBatch = measThreadBatch(filename)
    try:
        thMeasBatch.start()
    except Exception as exc:
        print("Warning:Exception:",type(exc),exc.args)
        lstatus.setText("Meausure Status: Exception")
    else: 
        print("Batch Thread Started Successfully!")
    lstatus.setText("Meausure Status: batch Measuring")
        
    
def runBatch():
    global filename,thMeas
    _updateConfig()
    
    IoutText = cbatch_select.currentText()
    if IoutText == "Iout A" : Iout = 0 ;
    if IoutText == "Iout B" : Iout = 1 ;
    if IoutText == "Iout C" : Iout = 2 ;
    if IoutText == "Iout D" : Iout = 3 ;
    if IoutText == "All" : Iout = 0 ;

   
    print("start batch run....",Iout)
    GndACb.setChecked((Iout != 0))
    GndBCb.setChecked((Iout != 1))
    GndCCb.setChecked((Iout != 2))
    GndDCb.setChecked((Iout != 3))
    
    _switch()
    
    rangeMin = 0
    rangeMax = 9
    if IoutText == "All" :
        rangeMin = 1
        rangeMax = 26

    for m in range(rangeMin,rangeMax):
        if IoutText == "All" : 
            PUT = "CH" + str(m)
        else: 
            PUT = IoutCmdList[m][Iout]
        print('mode:', m, ' PUT:', PUT)
        if PUT == 'GND' or PUT == 'GR':
            print('PUT is ', PUT, '  continue....')
            continue
        pad = PUT.replace("CH","")
        if len(pad) == 1: 
            pad = "0" + pad  
        newfile = filename[0:filename.rfind('/')+1] + pad + ".csv"    
        print('measuring pad:', PUT, 'save as:', newfile)
        
        lswitch.setText(str(m))
        _switch()
        
        t1.setText(newfile)
        _updateConfig()

        if os.path.exists(newfile):
            if True : # doOverwrite
                print("file:",newfile,"already exists, overwrite is enabled, old file is deleted......")
                cmd = "del "+newfile.replace("/","\\")
                os.system(cmd)
                pass
            else:
                print("file:",newfile,"already exists, overwrite not enabled, continue......")
                continue        
        
        _measure()
     
        # print("before creating measure thread")
        # thMeas = measThread(filename,voltageMax,mode)
        # print("after creating measure thread")
        # try:
            # print("before start measure thread")
            # thMeas.start()
            # print("after start measure thread")
        # except Exception as exc:
            # print("Warning:Exception:",type(exc),exc.args)
            # lstatus.setText("Meausure Status: Exception")
        # else: 
            # print("Thread Started Successfully!")
        # lstatus.setText("Meausure Status: Measuring")
        # sleep(1)
        # startGraph()
        # print("after started graph")
        # print("after measure exec")
        
        sleep(3)
        #thMeas.join()
        while 'thMeas' in globals() and not thMeas.dead:
            print("thMeas not dead.....")
            sleep(5)
            # #app.processEvents()
            
        print("thMeas not dead.....")
        del thMeas
        
#            sleep(5)
#            if not thMeas.dead:
#                print(". measure ongoing")
#                #update_plt()
#            else:
#                print("Deleting previous dead measure thread.....")   
#                del thMeas
        print('finished measuring pad:', PUT)
        #uploadData()
        

class myThread (QThread):
    def __init__(self, filename):
        QThread.__init__(self)
        self.filename = filename
        self.name = filename

    def __del__(self):
        self.wait()

    def run(self):
        global thMeas
        print ("Thread Start:" + self.filename)
        try:
            monitorFile(self.name,self.filename)
        except Exception as exc:
            print("Warning: Exception during monitorFile \t",type(exc),exc.args)
        else:
            print ("Thread End " + self.filename)

            # while 'thMeas' in globals() and not thMeas.dead:
                # print("thMeas not dead.....")
                # sleep(5)
                # #app.processEvents()
                
            # print("thMeas not dead.....")
            # del thMeas           
            # if not thMeas.dead:
               # print(". measure ongoing")
               # #update_plt()
            # else:
               # print("Deleting previous dead measure thread.....")   
               # del thMeas


class measThread (QThread):
    def __init__(self, filename,voltageMax,mode,enableParallel = False,enable15x15 = False,enable5x5VBD = False):
        QThread.__init__(self)
        self.filename = filename
        self.voltageMax = voltageMax
        self.mode = mode
        self.enablePara = enableParallel
        self.enable15x15_p = enable15x15
        self.enable5x5VBD_p = enable5x5VBD  
        self.dead = False
    def __del__(self):
        print("thread deleted...")
        self.wait()

    def run(self):
        measure.exitFlag=False
        print ("_measure Thread Start:" + self.filename,"mode:",self.mode,"voltageMax",self.voltageMax)
        try:
            if self.mode == "IV":
                if not enableParallel:
                    if enable5x5VBD:
                        measure.MeasureIV5x5VBD(self.voltageMax,self.filename)
                    else:
                        measure.MeasureIV(self.voltageMax,self.filename)
                else:
                    IoutText = cbatch_select.currentText()
                    if not enable15x15:
                        measure.MeasurePara5x5(self.voltageMax,self.filename,IoutText,"IV")
                    else:
                        measure.MeasurePara15x15(self.voltageMax,self.filename,IoutText,"IV")
            elif self.mode == "CV":
                if not enableParallel :
                    measure.MeasureCV(self.voltageMax,self.filename)
                else:
                    IoutText = cbatch_select.currentText()
                    if not enable15x15:
                        # measure.MeasurePara5x5(self.voltageMax,self.filename,IoutText,"CV")
                        measure.MeasurePara5x5CV(self.voltageMax,self.filename,IoutText)
                    else:
                        measure.MeasurePara15x15(self.voltageMax,self.filename,IoutText,"CV")
            else:
                print("Error: Wrong mode to measure : ",self.mode)               
        except FileNotFoundError as exc:
            print("Warning: FileNotFoundError during measure \t",type(exc),exc.args)
            print("Path:",self.filename," doesn't exist, **please shutdown measure and reset a valid filename**")
        except Exception as exc:
            print("Warning: Exception during measure \t",type(exc),exc.args)
        else:
            lstatus.setText("Meausure Status:"+coloredFont("Measure Finished","green") )
            stopGraph()
            self.dead = True 
            print ("_measure Thread End :" + self.filename)
            uploadData()
        print("measured thread exiting now.....")
        self.quit()
        

class measThreadBatch (QThread):
    def __init__(self, filename):
        QThread.__init__(self)
        self.filename = filename
        
    def __del__(self):
        self.wait()

    def run(self):
        print ("Batch Thread Start:" + self.filename)
        try:
            runBatch()
        except Exception as exc:
            print("Warning: Exception during runBatch \t",type(exc),exc.args)
        else:
            print ("Batch Thread End" + self.filename)    

# class monThread (QThread):
#     def __init__(self, filename):
#         QThread.__init__(self)
#         self.filename = filename
#         self.dead = False
#     def __del__(self):
#         self.wait()

#     def run(self):
#         print('Monitor Output:',commands.getstatus('python3 '+filename))


"""
-------------GUI--Below------------
"""


"""-------------Create Window and App-------------"""

if not QtGui.QApplication.instance():
    app = QtGui.QApplication([])
else:
    app = QtGui.QApplication.instance()

win = QtGui.QWidget()
win.setWindowTitle("USTC LGAD IV/CV Test Programme")
win.resize(1000, 800)
win.move(200,10)
layout = QtGui.QGridLayout()
win.setLayout(layout)

layoutMon2 = QtGui.QGridLayout()

layoutControl = QtGui.QGridLayout()
layoutConfig =  QtGui.QGridLayout()
layoutSwitch =  QtGui.QGridLayout()
layoutMessage = QtGui.QGridLayout()


layoutCol1 =  QtGui.QGridLayout()
layoutCol2 =  QtGui.QGridLayout()
layoutCol3 =  QtGui.QGridLayout()


"""-------------Initialize Graph-------------"""



buffersize=100
p1 = pg.PlotWidget()
p1.setRange(yRange=[-1, 1])
p1.addLegend()
p1.showGrid(x=True, y=True, alpha=0.8)
p1.setLabel('left', 'Current [A]')
p1.setLabel('bottom', 'Bias',units = "V")
p1.setFixedHeight(350)

p2 = pg.PlotWidget()
p2.setRange(yRange=[-1, 1])
p2.addLegend()
p2.showGrid(x=True, y=True, alpha=0.8)
p2.setLabel('left', 'doping [cm-3]')
#p2.setFixedHeight(120)

#p2.hide()

curve1 = p1.plot(pen='y', name="I_total")
curve2 = p1.plot(pen='g', name="I_ch1")
curve3 = p1.plot(pen='b', name="I_ch2")

curve4 = p2.plot(pen='y', name="doping")





p3 = pg.PlotWidget()
p3.setRange(yRange=[-1, 1])
p3.addLegend()
p3.showGrid(x=True, y=True, alpha=0.8)
p3.setLabel('left', 'Y',units = "A.U.")
p3.setLabel('bottom', 'time',units = "seconds")
p3.setFixedHeight(200)
curve5 = p3.plot(pen='y', name="")

p4 = pg.PlotWidget() 
p4.setRange(yRange=[-1, 1])
p4.addLegend()
p4.showGrid(x=True, y=True, alpha=0.8)
p4.setLabel('left', 'Err',units = "%")
p4.setLabel('bottom', 'time',units = "seconds")
p4.setFixedHeight(200)
curve6 = p4.plot(pen='y', name="")


p5 = pg.PlotWidget()
p5.setRange(yRange=[-1, 1])
p5.addLegend()
p5.showGrid(x=True, y=True, alpha=0.8)
p5.setLabel('left',   'HV Bias',  units = "V")
p5.setLabel('bottom', 'time',units = "seconds")
p5.setFixedHeight(200)
curve7 = p5.plot(pen='y', name="")

#p3.hide()

pi = p1.getPlotItem()
pi2 = p2.getPlotItem()
pi3 = p3.getPlotItem()
pi4 = p4.getPlotItem()
pi5 = p5.getPlotItem()


pi.enableAutoRange()    
pi2.enableAutoRange()
pi3.enableAutoRange()
pi4.enableAutoRange()
pi5.enableAutoRange()

pi.setLogMode(None,True)
pi4.setLogMode(None,True)

timer = QTimer()
timer.timeout.connect(update_plt)
#timer.start(1)


timerMon = QTimer()
timerMon.timeout.connect(updateMon)

buffer1 = np.zeros(buffersize+1, int)
buffer2 = np.zeros(buffersize+1, int)
buffer3 = np.zeros(buffersize+1, int)
buffer4 = np.zeros(buffersize+1, int)
buffer5 = np.zeros(buffersize+1, int)
buffer6 = np.zeros(buffersize+1, int)

x = 0
dt=0.2

"""-------------Create Gui Componet-------------"""

bgraph = QtGui.QPushButton("Graph")
bgraph.clicked.connect(startGraph)

bgraph_stop = QtGui.QPushButton("Stop Graph")
bgraph_stop.clicked.connect(stopGraph)


blcrcorr_open = QtGui.QPushButton("LCRCorrOpen")
blcrcorr_open.clicked.connect(LCRCorrOpen)

blcrcorr_short = QtGui.QPushButton("LCRCorrShort")
blcrcorr_short.clicked.connect(LCRCorrShort)

bsetfile = QtGui.QPushButton("Set File")
bsetfile.clicked.connect(_updateConfig)



binit = QtGui.QPushButton("Init / Set")
binit.clicked.connect(_init)

bshutdown = QtGui.QPushButton("Shutdown / Abort")
bshutdown.clicked.connect(_shutdown)

bmeasure = QtGui.QPushButton("Measure")
bmeasure.clicked.connect(_measure)

bswitch = QtGui.QPushButton("Switch")
bswitch.clicked.connect(_switch)

bupload = QtGui.QPushButton("Upload Data")
bupload.clicked.connect(uploadData)

bSaveGraph = QtGui.QPushButton("Save Graph")
bSaveGraph.clicked.connect(_saveGraph)


brunbatch = QtGui.QPushButton("Run Batch Serial")
brunbatch.clicked.connect(_runBatch)

ustcid_next = QtGui.QPushButton("Next.")
ustcid_next.clicked.connect(_ustcNext)

ustcid_replace = QtGui.QPushButton("R\&M")
ustcid_replace.clicked.connect(_ustcReplace)

ustcid_go = QtGui.QPushButton("Go")
ustcid_go.clicked.connect(_ustcGo)

bcontinue = QtGui.QPushButton("Continue")
bcontinue.clicked.connect(_Continue)

ustcid_next.setStyleSheet("background-color: orange")
ustcid_next.resize(500,600)
ustcid_replace.setStyleSheet("background-color: green")
ustcid_replace.resize(500,600)
ustcid_go.setStyleSheet("background-color: blue")
ustcid_go.resize(500,600)


linfo = QtGui.QLabel()
linfo.setText("File: "+filename)
linfo.setFrameStyle(QFrame.Panel |QFrame.Sunken)

lmode = QtGui.QLabel()
lmode.setText("Mode: None")

lstatus = QtGui.QLabel()
lstatus.setText("Status: Nothing")

lconfig = QtGui.QLabel()
lconfig.setText("Configure: Nothing")

lmonitor = QtGui.QLabel()
lmonitor.setText('Current Voltage:  '+redFont('N/A'))


lbfrequency = QtGui.QLabel()
lbfrequency.setText("CV Freq [kHz]:")

lbvoltage_min = QtGui.QLabel()
lbvoltage_min.setText("VMin [V]:")

lbprecision = QtGui.QLabel()
lbprecision.setText("Precision:")

lbcompliance = QtGui.QLabel()
lbcompliance.setText("Compliance [A]:")

lbvoltage = QtGui.QLabel()
lbvoltage.setText("VMax [V]:")

lbstep = QtGui.QLabel()
lbstep.setText("Step [V]:")

lbdelay = QtGui.QLabel()
lbdelay.setText("Delay [s]:")

lbswitch = QtGui.QLabel()
lbswitch.setText("Sw. Mode:")

lbswitch_conf = QtGui.QLabel()
lbswitch_conf.setText("Sw. Conf: N/A")

lbswitch_status = QtGui.QLabel()
lbswitch_status.setText("Sw. Status: N/A")

lbatchselect = QtGui.QLabel()
lbatchselect.setText("Batch Select:")



lbvoltage_fin = QtGui.QLabel()
lbvoltage_fin.setText("V_finish [V]:")
#t1 = QtGui.QLineEdit("IV/Single/T3.2_2.5E15/W17/01.csv")

t1 = QtGui.QLineEdit("IVsas")

t1.setFixedWidth(400) 

labelFixedWidth = 140
inputFixedWidth = 100

lfrequency = QtGui.QLineEdit()
lfrequency.setText("10")
lfrequency.setFixedWidth(inputFixedWidth)
lbfrequency.setFixedWidth(labelFixedWidth)

lvoltage_min = QtGui.QLineEdit()
lvoltage_min.setText("0")
lvoltage_min.setFixedWidth(inputFixedWidth)
lbvoltage_min.setFixedWidth(labelFixedWidth)

lprecision = QtGui.QLineEdit()
lprecision.setText("1e-3")
lprecision.setFixedWidth(inputFixedWidth)
lbprecision.setFixedWidth(labelFixedWidth)

lcompliance = QtGui.QLineEdit()
lcompliance.setText("5e-6")
#lcompliance.setText("1e-6")
lcompliance.setFixedWidth(inputFixedWidth)
lbcompliance.setFixedWidth(labelFixedWidth)

lvoltage = QtGui.QLineEdit()
lvoltage.setText("300")
lvoltage.setFixedWidth(inputFixedWidth)
lbvoltage.setFixedWidth(labelFixedWidth)

lstep = QtGui.QLineEdit()
#lstep.setText("2.0")
lstep.setText("1.0")
lstep.setFixedWidth(inputFixedWidth) 
lbstep.setFixedWidth(labelFixedWidth) 

ldelay = QtGui.QLineEdit()
ldelay.setText("0.2")
ldelay.setFixedWidth(inputFixedWidth) 
lbdelay.setFixedWidth(labelFixedWidth) 

lswitch = QtGui.QLineEdit()
lswitch.setText("1")
lswitch.setFixedWidth(inputFixedWidth)
lbswitch.setFixedWidth(labelFixedWidth)



lvoltage_fin = QtGui.QLineEdit()
lvoltage_fin.setText("0")
lvoltage_fin.setFixedWidth(inputFixedWidth)
lbvoltage_fin.setFixedWidth(labelFixedWidth)




cbatch_select = QtGui.QComboBox()
cbatch_select.addItem("All")
cbatch_select.addItem("Iout A")
cbatch_select.addItem("Iout B")
cbatch_select.addItem("Iout C")
cbatch_select.addItem("Iout D")




ustcid_select = QtGui.QComboBox()
ustcid_select.resize(600,200)
for id in PositionMap:
    ustcid_select.addItem(id)
# ustcid_select.addItem("IoutA")
# ustcid_select.addItem("IoutB")
# ustcid_select.addItem("IoutC")
# ustcid_select.addItem("IoutD")



"""-------------Place Gui Componet-------------"""

layout.addWidget(p1, 0, 0, 1, 6)


# layout.addWidget(p2, 1, 0, 1, 1)
# layout.addWidget(p3, 1, 1, 1, 3)

layout.addLayout(layoutMon2, 1, 0)
layout.addLayout(layoutCol1, 2, 0)
layout.addLayout(layoutCol2, 3, 0)
layout.addLayout(layoutCol3, 4, 0)

layoutMon2.addWidget(p5, 0, 0)  #HV
#layoutMon2.addWidget(p2, 0, 0) #doping profile
layoutMon2.addWidget(p3, 0, 1)  # Cp
layoutMon2.addWidget(p4, 0, 2)  # Cp Error
layoutMon2.setHorizontalSpacing(2)
layout.setHorizontalSpacing(0)
# layoutMon2.setFixedHe(200)

layoutCol1.addLayout(layoutControl, 0,0)

layoutControl.addWidget(bgraph, 0, 0)
layoutControl.addWidget(t1, 0, 1)

layoutControl.addWidget(bgraph_stop,     0, 2)
layoutControl.addWidget(blcrcorr_open,   0, 3)
layoutControl.addWidget(blcrcorr_short,  0, 4)

layoutControl.addWidget(binit,           1, 0)
layoutControl.addWidget(bmeasure,        1, 1)
layoutControl.addWidget(bshutdown,       1, 2)
layoutControl.addWidget(bupload,         1, 3)
layoutControl.addWidget(bSaveGraph,         1, 4)


groupBoxConfig = QGroupBox("Configure")
groupBoxConfig.setLayout(layoutConfig)
layoutCol2.addWidget(groupBoxConfig, 0,0)
                    
layoutConfig.addWidget(lbprecision,      4, 2)
layoutConfig.addWidget(lprecision,       4, 3)

layoutConfig.addWidget(lbcompliance,     5, 2)
layoutConfig.addWidget(lcompliance,      5, 3)


layoutConfig.addWidget(lbfrequency,      3, 2)
layoutConfig.addWidget(lfrequency,       3, 3)
layoutConfig.addWidget(lbvoltage_min,    3, 0)
layoutConfig.addWidget(lvoltage_min,     3, 1)

layoutConfig.addWidget(lbvoltage,        4, 0)
layoutConfig.addWidget(lvoltage,         4, 1)

layoutConfig.addWidget(lbstep,           5, 0)
layoutConfig.addWidget(lstep,            5, 1)

layoutConfig.addWidget(lbdelay,          6, 0)
layoutConfig.addWidget(ldelay,           6, 1)


radiobutton = QtGui.QRadioButton("IV")
radiobutton.setChecked(True)
radiobutton.mode = "IV"
radiobutton.toggled.connect(_setmode)
layoutConfig.addWidget(radiobutton,      7, 0)

radiobutton = QtGui.QRadioButton("CV")
radiobutton.mode = "CV"
radiobutton.toggled.connect(_setmode)
layoutConfig.addWidget(radiobutton,      8, 0)

radiobuttonCb = QCheckBox("UseChannelB")
radiobuttonCb.setChecked(True)
#radiobuttonCb.mode = "UseChannelB"
radiobuttonCb.stateChanged.connect(_setmodeCb)
layoutConfig.addWidget(radiobuttonCb,    7, 1)

dostablebutton = QCheckBox("DoStable")
dostablebutton.stateChanged.connect(_dostable)
layoutConfig.addWidget(dostablebutton,   8, 1)


layoutSwitch.addWidget(lbswitch,        0, 0)
layoutSwitch.addWidget(lswitch,         0, 1)
layoutSwitch.addWidget(bswitch,         0, 2)


bswitch.setFixedWidth(inputFixedWidth)

GndACb = QCheckBox("GND A")
GndACb.setChecked(True)
GndBCb = QCheckBox("GND B")
GndBCb.setChecked(True)
GndCCb = QCheckBox("GND C")
GndCCb.setChecked(True)
GndDCb = QCheckBox("GND D")
GndDCb.setChecked(True)

layoutSwitch.addWidget(GndACb,          1, 0)
layoutSwitch.addWidget(GndBCb,          2, 0)
layoutSwitch.addWidget(GndCCb,          1, 1)
layoutSwitch.addWidget(GndDCb,          2, 1)

layoutSwitch.addWidget(lbatchselect,    3, 0)
layoutSwitch.addWidget(cbatch_select,   4, 0)
layoutSwitch.addWidget(brunbatch,       5, 0)
# layoutSwitch.addWidget(ustcid_select,       5, 0)
# layoutSwitch.addWidget(ustcid_next,       5, 1)
# layoutSwitch.addWidget(ustcid_replace,       5, 2)
# layoutSwitch.addWidget(ustcid_go,       6, 2)

layoutSwitch.addWidget(lbswitch_conf,    3, 1)
layoutSwitch.addWidget(lbswitch_status,  4, 1)



layoutMessage.addWidget(linfo,            0, 0)
layoutMessage.setColumnMinimumWidth(0,400)
groupBoxMessage = QGroupBox("Message")
groupBoxMessage.setLayout(layoutMessage)


layoutMessage.addWidget(lconfig,          1, 0)
layoutMessage.addWidget(lmode,            2, 0)
layoutMessage.addWidget(lstatus,          3, 0)
layoutMessage.addWidget(lmonitor,         4, 0)


groupBoxSwitch = QGroupBox("Switch")
groupBoxSwitch.setLayout(layoutSwitch)
layoutCol2.addWidget(groupBoxSwitch, 0,1)
layoutCol3.addWidget(groupBoxMessage, 0,0)
layout.setVerticalSpacing(6)
layoutConfig.setVerticalSpacing(6)
#layout.setHorizontalSpacing(6)

EnUploadCb = QCheckBox("Enable Upload")
EnUploadCb.setChecked(True)
layoutConfig.addWidget(EnUploadCb, 7, 2)

EnSwitchCb = QCheckBox("Enable Switch")
EnSwitchCb.setChecked(True)
layoutConfig.addWidget(EnSwitchCb, 8, 2)

radiobuttonParaCb = QCheckBox("Enable Parallel")
radiobuttonParaCb.setChecked(enableParallel)
radiobuttonParaCb.stateChanged.connect(_setParallelCb)
layoutConfig.addWidget(radiobuttonParaCb, 7, 3)

radiobuttonParaCb_15x15 = QCheckBox("Enable 15x15")
radiobuttonParaCb_15x15.setChecked(enable15x15)
radiobuttonParaCb_15x15.stateChanged.connect(_set15x15Cb)
layoutSwitch.addWidget(radiobuttonParaCb_15x15, 5, 1)

radiobuttonParaCb_5x5VBD = QCheckBox("5x5 VBD")
radiobuttonParaCb_5x5VBD.setChecked(enable5x5VBD)
radiobuttonParaCb_5x5VBD.stateChanged.connect(_set5x5VBDCb)
layoutSwitch.addWidget(radiobuttonParaCb_5x5VBD, 5, 2)

# layoutSwitch.addWidget(lbvoltage_start,    3, 2)
layoutSwitch.addWidget(bcontinue,       3, 2)
layoutSwitch.addWidget(lbvoltage_fin,    4, 2)
# layoutSwitch.addWidget(lvoltage_start,     3, 3)
layoutSwitch.addWidget(lvoltage_fin,     4, 3)

LogYCb = QCheckBox("LogY")
LogYCb.setChecked(True)
LogYCb.stateChanged.connect(_setLogy)
layoutConfig.addWidget(LogYCb, 8, 3)


t1.setText(fetchFileNameHist())

"""-------------Some variable need initial value-------------"""
y5 = []
x5 = []
y6 = []
x6 = []
y7 = []
x7 = []


"""-------------Show window and start app-------------"""
win.show()

try:
    app.exec_()
except Exception as exc:
    print("Exception in app.exec()",type(exc))
    print(exc.args)
