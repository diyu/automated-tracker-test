# -*- coding: utf-8 -*-
# Developed by USTC
import serial
from PyQt5 import QtGui, QtCore 
from PyQt5.QtCore import QThread,QTimer,Qt
from PyQt5.QtWidgets import QMessageBox, QCheckBox,QGroupBox,QFrame
import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import sys
import math
import tail
import os
#import commands
import time

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='COM6',
    baudrate=19200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS
)

def redFont(s):
    return '<font size="6" color="red">  '+str(s)+'  </font>'
def greenFont(s):
    return '<font size="6" color="green">  '+str(s)+'  </font>'
def blueFont(s):
    return coloredFont(s,color="blue")
def coloredFont(s,color="red"):
    return '<font size="6" color="'+color+'">  '+str(s)+'  </font>'

def info(msg):
    print("[INFO] ",msg)
def error(msg):
    print("[ERROR] ",msg)
def warn(msg):
    print("[WARNNING] ",msg)
def debug(msg):
    print("[DEBUG] ",msg)

x = []
y1 = []
y2 = []
y3 = []
y4 = []

timeZero = time.time()

ina = 1


def init():
    ser.isOpen()
def shutdown():
    ser.close()

def exec(cmd,delay=0.05):
    cmd = cmd + '\r\n'

    ser.write(cmd.encode())
    out = b''
    outStr = ''
    time.sleep(delay)
    while ser.inWaiting() > 0:
            out += ser.read(1)
    if out != '':
            outStr = str(out).replace("\\r\\n\'","")
            outStr = outStr.replace("b'","")
            #print (">>" , out)
            #print (">>" , outStr)
    return outStr

#---Activate/Deactivate temperature control with Peltier.
def drive(status = "ON"):
    result = ''
    if(status == "ON"):
        result = exec("TCC,1")
    if(status == "OFF"):
        result = exec("TCC,0")
    else:  
        result = 'Error'
        print("wrong status needed.")
    return result

def setTemperature(target):
    return exec("SVS,"+str(target))

def showTemperature():
    return exec("TMR")

def showTemperatureSetting():
    return exec("SVR")

def showStatus():
    return exec("STR")
     

def update_plt():
    global x,y1,y2,y3,y4
    
    timestamp =  time.time()
    TStr = showTemperature()
    TStrArray = TStr.split(',')

    TSettingStr = showTemperatureSetting()
    TSettingStrArray = TSettingStr.split(',')

    Tmeas = float(TStrArray[0])
    Imeas = float(TStrArray[1])
    Vmeas = float(TStrArray[2])
    DTC = float(TStrArray[3])
    
    Tset = float(TSettingStrArray[0])
    #print(TStr)
    print("TStrArray",TStrArray)
    print("TSettingStrArray",TSettingStrArray)

    x.append(timestamp - timeZero)
    y1.append(Tmeas)
    y2.append(Tset)
    y3.append(Imeas)
    y4.append(Vmeas)


    curve1.setData(x,y1)
    curve2.setData(x,y2)
    curve3.setData(x,y3)
    curve4.setData(x,y4)
    monStr = "T(meas): "+ redFont(round(Tmeas,2)) + " C, T(set):" + blueFont(Tset)  + " C, I:" + blueFont(Imeas)  + " A, V:" + blueFont(Vmeas)  + " V, AbnormalCode(DTC):" + redFont(DTC) 

    lmonitor.setText(monStr)
    
    statusCode = showStatus()
    statusMsg = 'Unknown'
    if statusCode == "00": statusMsg = 'No temperature control or auto-tuning';
    if statusCode == "01": statusMsg = 'In temperature control';
    if statusCode == "02": statusMsg = 'In auto-tuning';
    if statusCode == "04": statusMsg = 'Auto-tuning successful completion';
    if statusCode == "08": statusMsg = 'Auto-tuning abnormal termination';
    if statusCode == "10": statusMsg = 'Stabilized temperature';
    
    statusMsgShort = 'Unknown'
    if statusCode == "00": statusMsgShort = coloredFont("Cooling OFF","orange") ;
    if statusCode == "01" or statusCode == "10": statusMsgShort = coloredFont("Cooling ON","green") ;
    monStr2 = "Status:" + blueFont(statusCode)+ " "+ statusMsgShort + "("+statusMsg+")"

    lmonitor2.setText(monStr2)



def _driveOn():
    drive("ON")
def _driveOff():
    drive("OFF")
def _quit():
    shutdown()
    timer.stop()
    exit()

def _setTargetTemperature():
    #global targetTemperature
    targetTemperature = float(lTSet.text())
    if targetTemperature > -31 and targetTemperature < 30:
        info("Setting temperature target value to : "+str(targetTemperature))
        info("return: "+setTemperature(targetTemperature))
    else:
        warn("Temperature "+str(targetTemperature)+" is out of range [-30,30], it won't be set")



if not QtGui.QApplication.instance():
    app = QtGui.QApplication([])
else:
    app = QtGui.QApplication.instance()

win = QtGui.QWidget()
win.setWindowTitle("Temperature Control")
win.resize(900, 700)
win.move(500,200)
layout = QtGui.QGridLayout()
win.setLayout(layout)


buffersize=100
#Plot 1 for temperature monitor
p1 = pg.PlotWidget()
p1.setRange(yRange=[-1, 1])
p1.addLegend()
p1.showGrid(x=True, y=True, alpha=0.8)
p1.setLabel('left', 'Temperature',units = "C")
p1.setLabel('bottom', 'Time',units = "s")
p1.setFixedHeight(350)


curve1 = p1.plot(pen='y', name="T. measured")
curve2 = p1.plot(pen='g', name="T. setting")
p1.enableAutoRange() 


#Plot 2 for Peltier current & voltage monitor
p2 = pg.PlotWidget()
p2.setRange(yRange=[-1, 1])
p2.addLegend()
p2.showGrid(x=True, y=True, alpha=0.8)
p2.setLabel('left', 'Current/Voltage',units = "A.U.")
p2.setLabel('bottom', 'Time',units = "s")
p2.setFixedHeight(350)
curve3 = p2.plot(pen='y', name="Peltier current[A]")
curve4 = p2.plot(pen='g', name="Peltier voltage[V]")
p2.enableAutoRange() 


timer = QTimer()
timer.timeout.connect(update_plt)



"""------------Create Buttons and Widget------------"""

bdriveOn = QtGui.QPushButton("Drive ON")
bdriveOn.clicked.connect(_driveOn)

bdriveOff = QtGui.QPushButton("Drive Off")
bdriveOff.clicked.connect(_driveOff)

bComOn = QtGui.QPushButton("COM On")
bComOn.clicked.connect(init)

bComOff = QtGui.QPushButton("COM Off")
bComOff.clicked.connect(shutdown)

bQuit = QtGui.QPushButton("Quit")
bQuit.clicked.connect(_quit)


lmonitor = QtGui.QLabel()
lmonitor.setText('Initializing...  ')

lmonitor2 = QtGui.QLabel()
lmonitor2.setText('Initializing...  ')

lTSetlabel = QtGui.QLabel()
lTSetlabel.setText('Target Temperature [C]:')

lTSet = QtGui.QLineEdit()
lTSet.setText("20")

bTSet = QtGui.QPushButton("Set")
bTSet.clicked.connect(_setTargetTemperature)

layoutCol1 =  QtGui.QGridLayout()
layoutCol2 =  QtGui.QGridLayout()

layout.addWidget(p1,1,0)
layout.addWidget(p2,2,0)

layout.addLayout(layoutCol1, 3, 0)

layoutCol1.addWidget(bdriveOn,  0, 1)
layoutCol1.addWidget(bdriveOff, 0, 2)
layoutCol1.addWidget(bQuit,     0, 3)

layout.addLayout(layoutCol2,4,0)
layout.addWidget(lmonitor,5,0)
layout.addWidget(lmonitor2,6,0)

layoutCol2.addWidget(lTSetlabel,     0, 1)
layoutCol2.addWidget(lTSet,     0, 2)
layoutCol2.addWidget(bTSet,     0, 3)

"""-------------Show window and start app-------------"""
win.show()

timer.start(500)
try:
    app.exec_()
    init()
except Exception as exc:
    shutdown()
    print("Exception in app.exec()",type(exc))
    print(exc.args)