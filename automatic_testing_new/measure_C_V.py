# Import necessary packages
from pymeasure.instruments.keithley import Keithley2400
from pymeasure.instruments.agilent import AgilentE4980
import numpy as np
import pandas as pd
from time import sleep
import c_v_tools

# Set the input paremeters
tolerence = 0.1

# Connect and reset the instrument
sourcemeter_0 = Keithley2400("GPIB0::18::INSTR")
lcrmeter = AgilentE4980("GPIB0::17::INSTR")

# sourcemeter_0.write("*RST")

print("00    ", sourcemeter_0.ask("*IDN?"))
print("01    ", lcrmeter.ask("*IDN?"))

sourcemeter_0.write(":SOUR:FUNC VOLT")
# sourcemeter_0.write(":SENS:CURR:RANG:AUTO OFF")
# sourcemeter_0.write(":SENS:CURR:RANG 1E-4")


sleep(0.1)  # wait here to give the instrument time to react


sourcemeter_0.write("OUTPUT ON")
sleep(1.0)

c_v_tools.c_v_test(sourcemeter_0, lcrmeter, 0.5, 5)
# tools.csv_write(output)
# c_v_tools.voltage_down(sourcemeter_0, - 50)