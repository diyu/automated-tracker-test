# Import necessary packages
import csv
import time
from time import sleep
import numpy as np


# Save the results into a .csv file
def csv_write(
    output,
    file_name = "./readout_results/result_" + time.strftime("%Y%m%d_%H%M%S", time.localtime()) + ".csv"
):
    with open(file_name, "w", newline="") as csvfile:
        quivant_writer = csv.writer(csvfile)
        quivant_writer.writerow(["Voltage", "Capacity"])
        quivant_writer.writerows(output)

def get_capacity(
    capacity_from_sourcemeter
):
    capacity_str = ""
    for i in range(len(capacity_from_sourcemeter)):
        capacity_digit = capacity_from_sourcemeter[i]
        if capacity_digit == ",":
            break
        capacity_str += capacity_digit
    return float(capacity_str)

def wait(sourcemeter_0):
    I = 1
    while abs(I) > 1E-6:
        sleep(0.2)
        I = float(sourcemeter_0.ask(":READ?"))
    return 0

def program_pause(flag, sourcemeter_0):
    flag_box =[
        " voltage climbing. Enter 'n' to stop.",
        " voltage descending.",
        " capacity data taking."
    ]
    wait(sourcemeter_0)
    sus_flag = input("[INFO]Press enter to continue" + flag_box[flag])
    if sus_flag == "n":
        return 0
    else:
        return 1

def single_take(lcrmeter):
    lcrmeter.write("TRIG:IMM")
    capacity = get_capacity(lcrmeter.ask("FETC?"))
    return capacity

# Take the data output and judge the fluctuation
def data_take(
    lcrmeter,
    lag_time,
    tolerence
):
    retake = True
    retake_time = 0
    while retake:
        capacity = single_take(lcrmeter)
        sleep(lag_time)
        capacity_retake = single_take(lcrmeter)
        difference = (capacity - capacity_retake) / capacity * 100
        print("[INFO]Capacity = ", capacity, ", Retake = ", capacity_retake, ", Difference = ", difference)
        if abs(difference) < tolerence:
            retake = False
        sleep(lag_time)
        retake_time += 1
        if retake_time > 10:
            break
    return capacity


# One test circle
def test_circle(
    sourcemeter_0,
    lcrmeter,
    voltage,
    lag_time,
    tolerence
):
    sourcemeter_0.source_voltage = float(voltage)
    temp = float(sourcemeter_0.ask(":READ?"))
    wait(sourcemeter_0)
    capacity = data_take(lcrmeter, lag_time, tolerence)
    return capacity

#Voltage down
def voltage_down(
    sourcemeter_0,
    voltage_max
):
    for voltage in range(- voltage_max):
        voltage_apply = float(voltage + voltage_max + 1)
        wait(sourcemeter_0)
        sourcemeter_0.source_voltage = float(voltage_apply)
        print("[INFO]Voltage = ", voltage_apply)
    sourcemeter_0.write("OUTPUT OFF")

# C-V test
def c_v_test(
    sourcemeter_0,
    lcrmeter,
    lag_time,
    tolerence
):
    capacity_temp = 1
    print("\n\n\n [INFO]Data take begins. We love particle physics! ❤︎ \n\n\n")
    output = []
    voltage = - 1
    test_flag = 1
    while test_flag:
        capacity = test_circle(sourcemeter_0, lcrmeter, voltage, lag_time, tolerence)
        output.append([voltage, capacity])
        change = abs(capacity - capacity_temp) / capacity_temp * 100
        print("[INFO] Voltage = ", voltage, "Capacity = ", capacity, "Change = ", change)
        capacity_temp = capacity
        if change < 0.1 and voltage < -50:
            test_flag = 0
        voltage -= 1
        if voltage % 10 == 0:
            csv_write(output)
    print("\n\n\n [INFO]Data take all done. Now waiting for voltage to go down.\n\n\n")
    csv_write(output)
    voltage_down(sourcemeter_0, voltage)
    print("\n\n\n [INFO]Finished.\n\n\n")

