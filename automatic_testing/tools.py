# Import necessary packages
import csv
import time
from time import sleep
import numpy as np


# Save the results into a .csv file
def csv_write(
    output,
    file_name = "./readout_results/result_" + time.asctime(time.localtime(time.time())) + ".csv"
):
    with open(file_name, "w", newline="") as csvfile:
        quivant_writer = csv.writer(csvfile)
        quivant_writer.writerow(["Voltage", "Current", "Variance", "Retake"])
        quivant_writer.writerows(output)

# Derive the first channel from the whole output
def get_current(
    currents_from_sourcemeter
):
    current_str = ""
    for i in range(len(currents_from_sourcemeter)):
        current_digit = currents_from_sourcemeter[i]
        if current_digit == ",":
            break
        current_str += current_digit
    return float(current_str)

# Take the data output and judge the fluctuation
def data_take(
    sourcemeter_1, 
    dot_number, 
    tolerence, 
    interval,
    exceed = 2
):
    currents = []
    retake_flag = 0
    for i in range(dot_number):
        current = get_current(sourcemeter_1.ask(":READ?"))
        currents.append(current)
        sleep(interval)
    average, variance = np.mean(currents), np.var(currents)
    if variance > tolerence:
        for i in range(dot_number * exceed):
            current = get_current(sourcemeter_1.ask(":READ?"))
            currents.append(current)
            sleep(interval)
        average, variance, retake_flag = np.mean(currents), np.var(currents), 1
    return average, variance, retake_flag

# One test circle
def test_circle(
    sourcemeter_0, 
    sourcemeter_1, 
    pre_sleep_time, 
    post_sleep_time, 
    voltage, 
    dot_number, 
    interval
):
    sourcemeter_0.source_voltage = float(voltage)
    sleep(pre_sleep_time)
    current, variance, retake_flag = data_take(sourcemeter_1, dot_number, interval)
    sleep(post_sleep_time)
    return current, variance, retake_flag

# I-V test
def i_v_test(
    sourcemeter_0, 
    sourcemeter_1, 
    pre_sleep_time, 
    post_sleep_time, 
    voltage_max,
    dot_number = 10, 
    interval = 0.2
):
    print("\n\n\n [INFO]Data take begins. We love particle physics! ❤︎ \n\n\n")
    output = []
    for voltage in range(voltage_max):
        voltage *= -1
        current, variance, retake_flag = test_circle(sourcemeter_0, sourcemeter_1, pre_sleep_time, post_sleep_time, voltage, dot_number, interval)
        output.append([voltage, current, variance, retake_flag])
        flag_box = [
        " not taken.",
        " taken."
    ]
        print("[INFO]Voltage = ", voltage, "    Current = ", current, "    Variance = ", variance, "    extra points", flag_box[retake_flag])
    csv_write(output)
    print("\n\n\n [INFO]Data take all done. Now waiting for voltage to go down.\n\n\n")
    for voltage in range(voltage_max):
        voltage -= voltage_max
        voltage += 1
        sourcemeter_0.source_voltage = float(voltage)
        print("[INFO]Voltage = ", voltage)
        sleep(post_sleep_time)
    print("\n\n\n [INFO]Finished.\n\n\n")

