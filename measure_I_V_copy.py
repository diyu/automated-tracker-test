# Import necessary packages
from pymeasure.instruments.keithley import Keithley2400
import numpy as np
import pandas as pd
from time import sleep
import tools

# Set the input paremeters
pre_sleep_time = 0.5
post_sleep_time = 0.5
dot_number = 10
interval = 0.2

# Connect and reset the instrument
sourcemeter_0 = Keithley2400("GPIB0::18::INSTR")
sourcemeter_1 = Keithley2400("GPIB0::25::INSTR")

sourcemeter_0.write("*RST")
sourcemeter_1.write("*RST")

print("00    ", sourcemeter_0.ask("*IDN?"))
print("01    ", sourcemeter_1.ask("*IDN?"))

sourcemeter_0.write(":SOUR:FUNC VOLT")
sourcemeter_1.write("CONFigure:CURRent[:DC]")
sourcemeter_0.write(":SOUR:VOLT:ILIMIT 5e-6")

sleep(0.1)  # wait here to give the instrument time to react

sourcemeter_0.write("OUTPUT ON")
sleep(1.0)