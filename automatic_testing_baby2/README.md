# DarkSHINE silicon tracker automatic testing program

##### Dian YU(TDLI), Xiao YANG(USTC)

## Version Description
 This version is for **BABY2**. The key feature for this type of sensor is that the voltage is above 0 and goes up.

The code to automatically run the I-V and C-V tests for silicon trakers, configured to adapt TDLI lab environment, is provided here.

## Tools 
### csv_write
#### variables


| name   | type       | description                                                                                           |
|:------:|:----------:|:-----------------------------------------------------------------------------------------------------:|
| output | list(list) | used to save testing results, a record is constituted by voltage, current, variance, retake           |
| path   | str        | path to save the output .csv file, default to save into ./readout_results/ with localtime in the name |


#### function
To save recorde inside variable *output* into a csv with colomns *Voltage*, *Current*, *Variance*, *Retake*.


### get_current
#### variables



| name                      | type | description                         |
|:-------------------------:|:----:|:-----------------------------------:|
| currents_from_sourcemeter | str  | direct readout from sourcemeter6482 |

#### function
Direct readout from 6482 is a str with two channels' currents seperated with ",", cut from "," and transfer into float gets the first channel's current.



### data_take
#### variables

| name          | type  | description                                                                                          |
|:-------------:|:-----:|:----------------------------------------------------------------------------------------------------:|
| sourcemeter_1 |       | object to control                                                                                    |
| dot_number    | int   | currents taken for voltage                                                                           |
| tolerence     | float | if the sequence of the taken currents's variance is larger than tolerence, see it as not trustworthy |
| interval      | float | time between two dot takes                                                                           |
| exceed        | int   | if the sequence is not trustworthy, take more dots and exceed controls the number of extra dots      |

#### function
For a fixed voltage, take average and variance of the current sequence, and take more if the fluctuation is to much. If retake happens, the retake flag will turn to 1, instead of default 0.


### test_circle
#### variables

| variable        | type  | description                                        |
|:---------------:|:-----:|:--------------------------------------------------:|
| sourcemeter_0   |       | object to control(2470)                            |
| sourcemeter_1   |       | object to control(6482)                            |
| pre_sleep_time  | float | time before reading currents out after voltage set |
| post_sleep_time | float | time after data take                               |
| voltage         | float | voltage to take                                    |
| dot_number      | int   | see above                                          |
| interval        | float | see above                                          |

#### function
Combine voltage set and data_take function.

