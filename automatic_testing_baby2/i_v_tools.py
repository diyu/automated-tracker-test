# Import necessary packages
import csv
import time
from time import sleep
import numpy as np


# Save the results into a .csv file
def csv_write(
    output,
    file_name = "./readout_results/result_" + time.strftime("%Y%m%d_%H%M%S", time.localtime()) + ".csv"
):
    with open(file_name, "w", newline="") as csvfile:
        quivant_writer = csv.writer(csvfile)
        quivant_writer.writerow(["Voltage", "Current", "Variance", "Retake"])
        quivant_writer.writerows(output)

# Derive the first channel from the whole output
def get_current(
    currents_from_sourcemeter
):
    current_str = ""
    for i in range(len(currents_from_sourcemeter)):
        current_digit = currents_from_sourcemeter[i]
        if current_digit == ",":
            break
        current_str += current_digit
    return float(current_str)

def single_take(
    sourcemeter_0,
    voltage,
    dot_number,
    interval,
    output,
    currents = None
):
    if currents is None:
        currents = []
    for i in range(dot_number):
        current = get_current(sourcemeter_0.ask(":READ?"))
        if abs(current) > 0.1 ** 6:
            print("\n\n\n [ERROR]Current over 1000nA, emergency cut down on process! Please check the circuit and run again.\n\n\n")
            voltage_clear(sourcemeter_0, voltage, post_sleep_time = 0.2, output=output)
        currents.append(current)
        sleep(interval)
    return currents

# Take the data output and judge the fluctuation
def data_take(
    sourcemeter_0,
    voltage,
    dot_number, 
    tolerence, 
    interval,
    exceed = 2,
    output=None
):
    currents = []
    retake_time, retake_flag = 0, 0
    currents = single_take(sourcemeter_0, voltage, dot_number, interval, output=output)
    average, variance = np.mean(currents), np.var(currents)
    while retake_time < 3 and variance > tolerence:
        retake_time += 1
        print("[ERROR]Variance = ", variance, "over limit, retaking for the", retake_time, "time.")
        currents = single_take(sourcemeter_0, voltage, dot_number, interval, output=output)
        average, variance = np.mean(currents), np.var(currents)
    if retake_time >= 3:
        retake_currents = single_take(sourcemeter_0, voltage, dot_number * exceed, interval, output=output, currents=currents)
        average, variance, retake_flag = np.mean(retake_currents), np.var(retake_currents), 1
    return average, variance, retake_flag

# One test circle
def test_circle(
    sourcemeter_0, 
    pre_sleep_time, 
    post_sleep_time, 
    voltage, 
    dot_number, 
    tolerence,
    interval,
    output
):
    sourcemeter_0.source_voltage = float(voltage)
    temp = float(sourcemeter_0.ask(":READ?"))
    sleep(pre_sleep_time)
    current, variance, retake_flag = data_take(sourcemeter_0, voltage, dot_number, tolerence, interval, output=output)
    sleep(post_sleep_time)
    return current, variance, retake_flag

def voltage_clear(sourcemeter_0, pre_voltage, post_sleep_time, output = None, exit_flag = 1):
    for i in range(1, pre_voltage + 1):
        voltage = pre_voltage - i
        sourcemeter_0.source_voltage = float(voltage)
        temp = float(sourcemeter_0.ask(":READ?"))
        print("[INFO]Voltage = ", voltage)
        sleep(post_sleep_time)
    if output != None:
        csv_write(output)
    sourcemeter_0.write("OUTPUT OFF")
    if exit_flag == 1:
        exit(0)

def pause_program():
    user_input = input("\n\n\n [INFO]Data take all done. Do we need to take 10 more? (y/n): ")
    while user_input.lower() not in ['y', 'n']:
        user_input = input("[ERROR]Invalid input. Please enter 'y' or 'n': ")
    if user_input.lower() == 'y':
        return 1
    else:
        return 0


# I-V test
def i_v_test(
    sourcemeter_0,
    pre_sleep_time, 
    post_sleep_time, 
    voltage_max,
    dot_number,
    tolerence,
    interval,
    step_change_a,
    step_change_b,
    step
):
    if voltage_max <= 0 or type(voltage_max) != int or type(step) != int:
        print("\n\n\n [ERROR]Voltage must be positive and int. Please check the input and run again.\n\n\n")
        exit(0)
    if step_change_a >= step_change_b or step_change_b + step > voltage_max:
        print("\n\n\n [ERROR]Step change set error! Please check the input and run again.\n\n\n")
        exit(0)
    print("\n\n\n [INFO]Data take begins. We love particle physics! ❤︎ \n\n\n")
    output = []
    voltage = 0
    while voltage <= voltage_max:
        current, variance, retake_flag = test_circle(sourcemeter_0, pre_sleep_time, post_sleep_time, voltage, dot_number, tolerence, interval, output)
        output.append([voltage, current, variance, retake_flag])
        flag_box = [
        " not taken.",
        " taken."
        ]
        print("[INFO]Voltage = ", voltage, "    Current = ", current, "    Variance = ", variance, "    extra points", flag_box[retake_flag])
        if voltage >= step_change_a and voltage < step_change_b:
            voltage += step
        else:
            voltage +=1
    retake_index = pause_program()
    while retake_index == 1:
        print("\n\n\n [INFO]Overtaking is on proceeding.\n\n\n")
        for voltage in range(voltage_max + 1, voltage_max + 11):
            current, variance, retake_flag = test_circle(sourcemeter_0, pre_sleep_time, post_sleep_time, voltage, dot_number, tolerence, interval, output)
            output.append([voltage, current, variance, retake_flag])
            flag_box = [
                " not taken.",
                " taken."
            ]
            print("[INFO]Voltage = ", voltage, "    Current = ", current, "    Variance = ", variance, "    extra points", flag_box[retake_flag])
        voltage_max += 10
        retake_index = pause_program()
    print("\n\n\n [INFO]Data take all done. Now waiting for voltage to go down.\n\n\n")
    pre_voltage = voltage_max
    voltage_clear(sourcemeter_0, pre_voltage, post_sleep_time, output, 0)
    print("\n\n\n [INFO]Finished.\n\n\n")

