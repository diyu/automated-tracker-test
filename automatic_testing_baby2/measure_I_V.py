# Import necessary packages
from pymeasure.instruments.keithley import Keithley2400
import numpy as np
import pandas as pd
from time import sleep
import i_v_tools

# Set the input paremeters
pre_sleep_time = 0.2
post_sleep_time = 0
voltage_max = 500
dot_number = 5
interval = 0.2
tolerence = 1e-20
step_change_a = 100
step_change_b = 300
step = 10

# Connect and reset the instrument
sourcemeter_0 = Keithley2400("GPIB0::18::INSTR")
# sourcemeter_1 = Keithley2400("GPIB0::25::INSTR")

sourcemeter_0.write("*RST")
# sourcemeter_1.write("*RST")

print("00    ", sourcemeter_0.ask("*IDN?"))
# print("01    ", sourcemeter_1.ask("*IDN?"))

sourcemeter_0.write(":SOUR:FUNC VOLT")
sourcemeter_0.write(":SENS:CURR:RANG:AUTO OFF")
sourcemeter_0.write(":SENS:CURR:RANG 1E-4")
sourcemeter_0.write("CONF:CURR[:DC]")
sourcemeter_0.write(":SENS:CURR:RANG 200E-9")


sleep(0.1)  # wait here to give the instrument time to react

sourcemeter_0.write("OUTPUT ON")
sleep(1.0)

tools.i_v_test(sourcemeter_0, pre_sleep_time, post_sleep_time, voltage_max, dot_number, tolerence, interval, step_change_a, step_change_b, step)
# tools.csv_write(output)